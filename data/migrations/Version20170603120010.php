<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170603120010 extends AbstractMigration
{
    private $tables = [
        'brands', 'currencies', 'locations', 'offices', 'stores', 'warranty_period_units', 'native_categories', 'site_categories',
        'native_to_site_categories', 'products', 'product_remains', 'product_arrivals', 'product_dates_', 'product_prices', 'product_properties', 'product_property_groups',
        'product_property_units', 'product_properties_to_templates', 'product_templates_to_native_categories',
        'product_property_values', 'products_to_product_properties',
    ];

    /**
     * @param Schema $schema
     */
    public function up (Schema $schema)
    {
        for ($i = 1; $i <= 2; $i++) {
            $sql = "
                CREATE TABLE IF NOT EXISTS `brands_{$i}` (
                    `id` INT (11) NOT NULL,
                    `name` VARCHAR (255),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `currencies_{$i}` (
                    `id` INT (11) NOT NULL,
                    `name` VARCHAR (255),
                    `main_currency` INT (1) NOT NULL DEFAULT 0,
                    `rate` DECIMAL (8,2),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `locations_{$i}` (
                    `id` INT (11) NOT NULL,
                    `parent_id` INT (11),
                    `name` VARCHAR (255),
                    `zone_id` INT (11),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `offices_{$i}` (
                    `id` INT (11) NOT NULL,
                    `name` VARCHAR (255),
                    `location_id` INT (11),
                    `address` VARCHAR (255),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `stores_{$i}` (
                    `id` INT (11) NOT NULL,
                    `office_id` INT (11) NOT NULL,
                    `location_id` INT (11) NOT NULL,
                    `is_pickup` INT (1) NOT NULL DEFAULT 0,
                    `is_delivery` INT (1) NOT NULL DEFAULT 0,
                    PRIMARY KEY (`id`, `office_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `warranty_period_units_{$i}` (
                    `id` INT (11) NOT NULL,
                    `name` VARCHAR (255),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;

                CREATE TABLE IF NOT EXISTS `native_categories_{$i}` (
                    `id` INT (11) NOT NULL,
                    `parent_id` INT (11),
                    `name` VARCHAR (255),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `site_categories_{$i}` (
                    `id` INT (11) NOT NULL,
                    `parent_id` INT (11),
                    `original_id` INT (11),
                    `name` VARCHAR (255),
                    `url_name` VARCHAR (255),
                    `buy_name` VARCHAR (255),
                    `sort_index` INT (11),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `site_categories_{$i}` (
                    `id` INT (11) NOT NULL,
                    `parent_id` INT (11),
                    `level` INT (11),
                    `left_key` INT (11),
                    `right_key` INT (11),
                    `original_id` INT (11),
                    `name` VARCHAR (255),
                    `url_name` VARCHAR (255),
                    `buy_name` VARCHAR (255),
                    `sort_index` INT (11),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `native_to_site_categories_{$i}` (
                    `category_id` INT (11) NOT NULL,
                    `site_category_id` INT (11) NOT NULL,
                    PRIMARY KEY (`category_id`, `site_category_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `products_{$i}` (
                    `id` INT (11) NOT NULL,
                    `category_id` INT (11) NOT NULL,
                    `brand_id` INT (11) NOT NULL,
                    `original_product_id` INT (11),
                    `name` VARCHAR (255),
                    `description` TEXT,
                    `weight` DECIMAL (8, 2) NOT NULL DEFAULT 0,
                    `volume` DECIMAL (8, 2) NOT NULL DEFAULT 0,
                    `created_at` DATE NOT NULL,
                    `warranty_period` INT (11),
                    `warranty_period_unit_id` INT (11),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_remains_{$i}` (
                    `product_id` INT (11) NOT NULL,
                    `store_id` INT (11) NOT NULL,
                    `remains_quantity` INT (11),
                    `reserve_quantity` INT (11),
                    `sell_speed` DECIMAL (8,2),
                    PRIMARY KEY (`product_id`, `store_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_arrivals{$i}` (
                    `product_id` INT (11) NOT NULL,
                    `office_id` INT (11) NOT NULL,
                    `quantity` INT (11),
                    `arrival_date` DATETIME,
                    PRIMARY KEY (`product_id`, `office_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_dates_{$i}` (
                    `product_id` INT (11) NOT NULL,
                    `location_id` INT (11) NOT NULL,
                    `pickup_date` DATETIME NOT NULL,
                    `delivery_date` DATETIME NOT NULL,
                    `suborder_delivery_date` DATETIME NOT NULL,
                    PRIMARY KEY (`product_id`, `location_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_prices_{$i}` (
                    `product_id` INT (11) NOT NULL,
                    `price_category_id` INT (11) NOT NULL,
                    `zone_id` INT (11) NOT NULL,
                    `value` DECIMAL (8,2),
                    `prev_value` DECIMAL (8,2),
                    PRIMARY KEY (`product_id`, `price_category_id`, `zone_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_properties_{$i}` (
                    `id` INT (11) NOT NULL,
                    `parent_id` INT (11),
                    `name` VARCHAR (255),
                    `type_id` INT (11) NOT NULL,
                    `unit_id` INT (11) NOT NULL,
                    `kind_id` INT (11) NOT NULL,
                    `hidden` INT (1) NOT NULL DEFAULT 0,
                    `sort_values` INT (1) NOT NULL DEFAULT 0,
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_property_groups_{$i}` (
                    `id` INT (11) NOT NULL,
                    `name` VARCHAR (255),
                    `sort_index` INT (11),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_property_units_{$i}` (
                    `id` INT (11) NOT NULL,
                    `name` VARCHAR (255),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_properties_to_templates_{$i}` (
                    `template_id` INT (11) NOT NULL,
                    `property_id` INT (11) NOT NULL,
                    `property_group_id` INT (11),
                    `property_sort_index` INT (11),
                    PRIMARY KEY (`template_id`, `property_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                
                CREATE TABLE IF NOT EXISTS `product_templates_to_native_categories_{$i}` (
                    `template_id` INT (11) NOT NULL,
                    `category_id` INT (11) NOT NULL,
                    PRIMARY KEY (`template_id`, `category_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;
                               
                CREATE TABLE IF NOT EXISTS `product_property_values_{$i}` (
                    `id` INT (11) NOT NULL,
                    `parent_id` INT (11),
                    `property_id` INT (11) NOT NULL,
                    `value` VARCHAR (255),
                    `sort_index` INT (11),
                    PRIMARY KEY (`id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;  
                
                CREATE TABLE IF NOT EXISTS `products_to_product_properties_{$i}` (
                    `product_id` INT (11) NOT NULL,
                    `property_id` INT (11) NOT NULL,
                    `property_value_id` INT (11) NOT NULL,
                    `value_number` DECIMAL (8, 2),
                    `value_boolean` INT (1),
                    `value_string` VARCHAR (255),
                    `value_text` TEXT,
                    PRIMARY KEY (`product_id`, `property_id`, `property_value_id`)
                )ENGINE=MyISAM DEFAULT CHARSET = utf8;";

            $this->addSql ($sql);
        }

        foreach ($this->tables as $table) {
            foreach ([1 => 'active', 2 => 'passive'] as $key => $status) {
                $this->addSql ("INSERT IGNORE INTO `swap_tables` VALUES ('{$table}', '{$status}', '{$table}_{$key}')");
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down (Schema $schema)
    {
        foreach ($this->tables as $table) {
            for ($i = 1; $i <= 2; $i++) {
                $this->addSql ("DROP TABLE IF EXISTS `{$table}_{$i}`");
            }
        }

        $connection = $this->connection;

        $tables = implode (',', array_map (function ($name) use ($connection) { return $connection->quote ($name); }, $this->tables));

        $this->addSql ("DELETE FROM `swap_tables` WHERE `name` IN ($tables)");
    }
}
