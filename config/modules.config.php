<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Zend\Mvc\Plugin\FlashMessenger',
    'Zend\Serializer',
    'Zend\Navigation',
    'Zend\Log',
    'Zend\Cache',
    'Zend\Form',
    'Zend\InputFilter',
    'Zend\Filter',
    'Zend\Paginator',
    'Zend\Hydrator',
    'Zend\Session',
    'Zend\Router',
    'Zend\Validator',
    'UltimaClient',
    'SymfonyConsole',
    'Redis',
    'DoctrineModule',
    'DoctrineORMModule',
    'Application',
    'Ultima\Core',
    'Ultima\Replication',
    'Ultima\Catalog',
    'Ultima\Finance',
    'Ultima\Offices',
    'Ultima\Delivery',
    'Ultima\Order',
    'Ultima\Clients'
];
