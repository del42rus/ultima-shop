<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Application\Cache\RedisCacheFactory;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            UnderscoreNamingStrategy::class => InvokableFactory::class,
            Redis::class => RedisCacheFactory::class
        ],
    ],
    'doctrine' => [
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => __DIR__ . '/../../data/migrations',
                'name' => 'DoctrineMigrations',
                'namespace' => 'DoctrineMigrations',
                'table' => 'doctrine_migration_versions',
                'column' => 'version',
            ],
        ],
        'cache' => [
            'redis' => [
                'namespace' => \Doctrine\Common\Cache\RedisCache::class,
                'instance'  => Redis::class
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'query_cache'       => 'redis',
                'result_cache'      => 'redis',
                'metadata_cache'    => 'redis',
                'hydration_cache'   => 'redis',
                'naming_strategy' => UnderscoreNamingStrategy::class,
                'custom_hydration_modes' => [
                    'FetchColumnHydrator' => 'Application\Doctrine\Internal\Hydration\FetchColumnHydrator'
                ],
            ]
        ]
    ]
];
