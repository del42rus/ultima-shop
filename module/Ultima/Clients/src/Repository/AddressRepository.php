<?php

namespace Ultima\Clients\Repository;

use Ultima\Clients\Entity\Address;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\HydratorInterface;

class AddressRepository implements AddressRepositoryInterface
{
    protected $apiClient;

    protected $hydrator;

    public function __construct(ApiClient $apiClient, HydratorInterface $hydrator)
    {
        $this->apiClient = $apiClient;
        $this->hydrator = $hydrator;
    }

    public function getAddresses($useCache = true)
    {
        try {
            $result = $this->apiClient->get('GetDeliveryAddresses', [], $useCache);
        } catch (\Exception $e) {
            return null;
        }

        $addresses = [];

        foreach ($result as $address) {
            $addresses[] = $this->hydrator->hydrate((array) $address, new Address());
        }

        return $addresses;
    }

    public function getAddressById($addressId, $useCache = true)
    {
        try {
            $result = $this->apiClient->get('GetDeliveryAddresses', [], $useCache);
        } catch (\Exception $e) {
            return null;
        }

        foreach ($result as $address) {
            if ($address->Id == $addressId) {
                return $this->hydrator->hydrate((array) $address, new Address());
            }
        }

        return null;
    }

    public function save(Address $address)
    {
        $data = $this->hydrator->extract($address);

        try {
            if ($address->getId()) {
                $result = $this->apiClient->post('UpdateDeliveryAddress', $data);
            } else {
                $result = $this->apiClient->post('CreateDeliveryAddress', $data);
            }
        } catch (\Exception $e) {
            return false;
        }

        return $address->getId() ? $result->Success : (bool) $result->Id;
    }

    public function remove(Address $address)
    {
        try {
            $result = $this->apiClient->post('DeleteDeliveryAddress', ['Id' => $address->getId()]);
        } catch (\Exception $e) {
            return false;
        }

        return $result->Success;
    }
}