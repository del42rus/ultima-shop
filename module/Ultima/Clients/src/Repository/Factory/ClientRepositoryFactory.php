<?php

namespace Ultima\Clients\Repository\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Clients\Repository\ClientRepository;
use Ultima\Core\Hydrator\NamingStrategy\UpperCamelCaseNamingStrategy;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class ClientRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $apiClient = $container->get(ApiClient::class);
        $hydrator = (new ClassMethods())->setNamingStrategy(new UpperCamelCaseNamingStrategy());

        return new ClientRepository($apiClient, $hydrator);
    }
}