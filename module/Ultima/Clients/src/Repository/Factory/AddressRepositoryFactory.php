<?php

namespace Ultima\Clients\Repository\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Clients\Repository\AddressRepository;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class AddressRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $apiClient = $container->get(ApiClient::class);
        $hydrator = new ClassMethods();

        return new AddressRepository($apiClient, $hydrator);
    }
}