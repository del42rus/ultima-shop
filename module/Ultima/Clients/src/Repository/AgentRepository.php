<?php

namespace Ultima\Clients\Repository;

use Ultima\Clients\Entity\Agent;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\HydratorInterface;

class AgentRepository implements AgentRepositoryInterface
{
    protected $apiClient;

    protected $hydrator;

    public function __construct(ApiClient $apiClient, HydratorInterface $hydrator)
    {
        $this->apiClient = $apiClient;
        $this->hydrator = $hydrator;
    }

    public function getAgents($useCache = false)
    {
        try {
            $result = $this->apiClient->get('GetClientAgents', [], $useCache);
        } catch (\Exception $e) {
            return null;
        }

        $agents = [];

        foreach ($result as $agent) {
            $agents[] = $this->hydrator->hydrate((array) $agent, new Agent());
        }

        return $agents;
    }

    public function getAgentById($agentId, $useCache = false)
    {
        try {
            $result = $this->apiClient->get('GetClientAgents', [], $useCache);
        } catch (\Exception $e) {
            return null;
        }

        foreach ($result as $agent) {
            if ($agent->Id == $agentId) {
                return $this->hydrator->hydrate((array) $agent, new Agent());
            }
        }

        return null;
    }

    public function save(Agent $agent)
    {
        $data = $this->hydrator->extract($agent);

        try {
            if ($agent->getId()) {
                $result = $this->apiClient->post('UpdateAgentDetails', $data);
            } else {
                $result = $this->apiClient->post('CreateAgent', $data);
            }
        } catch (\Exception $e) {
            return false;
        }

        return $agent->getId() ? $result->Success : (bool) $result->Id;
    }
}