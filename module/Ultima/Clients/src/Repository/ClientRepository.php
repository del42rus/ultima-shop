<?php

namespace Ultima\Clients\Repository;

use Ultima\Clients\Entity\Client;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\HydratorInterface;

class ClientRepository implements ClientRepositoryInterface
{
    protected $apiClient;

    protected $hydrator;

    public function __construct(ApiClient $apiClient, HydratorInterface $hydrator)
    {
        $this->apiClient = $apiClient;
        $this->hydrator = $hydrator;
    }

    public function getClient($useCache = false)
    {
        try {
            $result = $this->apiClient->get('GetClientInfo', [], $useCache);
        } catch (\Exception $e) {
            return null;
        }

        if (!$result->Id) {
            return null;
        }

        $client = new Client();
        $this->hydrator->hydrate((array) $result, $client);

        return $client;
    }

    public function save(Client $client)
    {
        $data = $this->hydrator->extract($client);

        try {
            if ($client->getId()) {
                $result = $this->apiClient->post('UpdateClientInfo', $data);
            } else {
                $result = $this->apiClient->post('CreateClient', $data);
            }
        } catch (\Exception $e) {
            return false;
        }

        return $client->getId() ? $result->Success : (bool) $result->Id;
    }
}