<?php

namespace Ultima\Clients;

use Ultima\Clients\Authentication\Adapter\Factory\AdapterFactory;
use Ultima\Clients\Authentication\Factory\AuthenticationServiceFactory;
use Ultima\Clients\Authentication\Storage\Session;
use Ultima\Clients\Repository\AddressRepository;
use Ultima\Clients\Repository\AgentRepository;
use Ultima\Clients\Repository\ClientRepository;
use Ultima\Clients\Repository\Factory\AddressRepositoryFactory;
use Ultima\Clients\Repository\Factory\AgentRepositoryFactory;
use Ultima\Clients\Repository\Factory\ClientRepositoryFactory;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Ultima\Clients\Authentication\Adapter\WebService as WebServiceAdapter;
use Zend\ServiceManager\Factory\InvokableFactory;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'aliases' => [
                AuthenticationService::class => 'ultima.authenticationservice.default',
            ],
            'factories' => [
                ClientRepository::class => ClientRepositoryFactory::class,
                AddressRepository::class => AddressRepositoryFactory::class,
                AgentRepository::class => AgentRepositoryFactory::class,
                WebServiceAdapter::class => AdapterFactory::class,
                'ultima.authenticationservice.default' => AuthenticationServiceFactory::class,
                Session::class => InvokableFactory::class
            ]
        ];
    }
}
