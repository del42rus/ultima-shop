<?php

namespace Ultima\Clients\Entity;

class Agent
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $inn;

    /**
     * @var string
     */
    private $bic;

    /**
     * @var string
     */
    private $kpp;

    /**
     * @var string
     */
    private $okpo;

    /**
     * @var string
     */
    private $settlementAccount;

    /**
     * @var string
     */
    private $bankName;

    /**
     * @var string
     */
    private $bankCorrAccount;

    /**
     * @var string
     */
    private $email;

    /**
     * @var float
     */
    private $balanceAmount;

    /**
     * @var int
     */
    private $priceCategoryId;

    /**
     * @var float
     */
    private $maxReserveAmount;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * @param $inn
     */
    public function setInn($inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param $bic
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    }

    /**
     * @return string
     */
    public function getKpp()
    {
        return $this->kpp;
    }

    /**
     * @param $kpp
     */
    public function setKpp($kpp)
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string
     */
    public function getOkpo()
    {
        return $this->okpo;
    }

    /**
     * @param $okpo
     */
    public function setOkpo($okpo)
    {
        $this->okpo = $okpo;
    }

    /**
     * @return string
     */
    public function getSettlementAccount()
    {
        return $this->settlementAccount;
    }

    /**
     * @param $settlementAccount
     */
    public function setSettlementAccount($settlementAccount)
    {
        $this->settlementAccount = $settlementAccount;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getBankCorrAccount()
    {
        return $this->bankCorrAccount;
    }

    /**
     * @param $bankCorrAccount
     */
    public function setBankCorrAccount($bankCorrAccount)
    {
        $this->bankCorrAccount = $bankCorrAccount;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return float
     */
    public function getBalanceAmount(): float
    {
        return $this->balanceAmount;
    }

    /**
     * @param float $balanceAmount
     */
    public function setBalanceAmount(float $balanceAmount)
    {
        $this->balanceAmount = $balanceAmount;
    }

    /**
     * @return int
     */
    public function getPriceCategoryId(): int
    {
        return $this->priceCategoryId;
    }

    /**
     * @param int $priceCategoryId
     */
    public function setPriceCategoryId(int $priceCategoryId)
    {
        $this->priceCategoryId = $priceCategoryId;
    }

    /**
     * @return float
     */
    public function getMaxReserveAmount(): float
    {
        return $this->maxReserveAmount;
    }

    /**
     * @param float $maxReserveAmount
     */
    public function setMaxReserveAmount(float $maxReserveAmount)
    {
        $this->maxReserveAmount = $maxReserveAmount;
    }
}