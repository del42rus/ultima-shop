<?php

namespace Ultima\Clients\Authentication\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Clients\Authentication\Storage\Session;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Ultima\Clients\Authentication\Adapter\WebService as WebServiceAdapter;

class AuthenticationServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthenticationService($container->get(Session::class), $container->get(WebServiceAdapter::class));
    }
}