<?php

namespace Ultima\Clients\Authentication\Storage;

use Zend\Authentication\Storage\Session as ZendAuthSession;

class Session extends ZendAuthSession
{
    public function read()
    {
        return unserialize(parent::read());
    }
}