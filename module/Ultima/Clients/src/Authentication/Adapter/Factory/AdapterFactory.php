<?php

namespace Ultima\Clients\Authentication\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Clients\Authentication\Adapter\WebService as WebServiceAdapter;
use Ultima\Clients\Repository\ClientRepository;
use UltimaClient\Client\Client;
use Zend\ServiceManager\Factory\FactoryInterface;

class AdapterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $apiClient = $container->get(Client::class);
        $clientRepository = $container->get(ClientRepository::class);

        return new WebServiceAdapter($apiClient, $clientRepository);
    }
}