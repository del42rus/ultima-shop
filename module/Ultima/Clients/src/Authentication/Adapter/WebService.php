<?php

namespace Ultima\Clients\Authentication\Adapter;

use Ultima\Clients\Repository\ClientRepositoryInterface;
use UltimaClient\Client\Client;
use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\Authentication\Adapter\Exception;

class WebService extends AbstractAdapter
{
    protected $apiClient;

    protected $clientRepository;

    protected $authenticationResultInfo = null;

    public function __construct(Client $apiClient, ClientRepositoryInterface $clientRepository, $identity = null, $credential = null)
    {
        $this->apiClient = $apiClient;
        $this->clientRepository = $clientRepository;

        if (null != $identity) {
            $this->setIdentity($identity);
        }

        if (null != $credential) {
            $this->setCredential($credential);
        }
    }

    /**
     * @return AuthenticationResult
     */
    public function authenticate()
    {
        $this->setup();

        $result = $this->apiClient->post('SignInClientWithEmail', [
            'Email' => $this->identity,
            'Password' => $this->credential
        ]);

        if (!$result->Success) {
            $this->authenticationResultInfo['code'] = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;
            $this->authenticationResultInfo['messages'][] = 'Supplied credential is invalid.';

            return $this->createAuthenticationResult();
        }

        $identity = $this->clientRepository->getClient();

        if (!$identity) {
            $this->authenticationResultInfo['code'] = AuthenticationResult::FAILURE;
            $this->authenticationResultInfo['messages'][] = 'Agent isn\'t authenticated.';

            return $this->createAuthenticationResult();
        }

        $this->authenticationResultInfo['code'] = AuthenticationResult::SUCCESS;
        $this->authenticationResultInfo['identity'] = serialize($identity);
        $this->authenticationResultInfo['messages'][] = 'Authentication successful.';

        return $this->createAuthenticationResult();
    }

    /**
     * @throws Exception\RuntimeException
     */
    protected function setup()
    {
        if (null === $this->identity) {
            throw new Exception\RuntimeException(
                'A value for the identity was not provided prior to authentication with Api authentication adapter'
            );
        }

        if (null === $this->credential) {
            throw new Exception\RuntimeException(
                'A credential value was not provided prior to authentication with Api authentication adapter'
            );
        }

        $this->authenticationResultInfo = [
            'code' => AuthenticationResult::FAILURE,
            'identity' => $this->identity,
            'messages' => []
        ];
    }

    /**
     * @return \Zend\Authentication\Result
     */
    protected function createAuthenticationResult()
    {
        return new AuthenticationResult(
            $this->authenticationResultInfo['code'],
            $this->authenticationResultInfo['identity'],
            $this->authenticationResultInfo['messages']
        );
    }
}