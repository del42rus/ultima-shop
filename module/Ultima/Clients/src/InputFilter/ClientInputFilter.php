<?php

namespace Ultima\Clients\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

class ClientInputFilter extends InputFilter
{
    public function __construct()
    {
        $email = new Input('email');
        $email->setRequired(true)
            ->getValidatorChain()
            ->attach(new Validator\EmailAddress());

        $phone = new Input('phone');
        $phone->setRequired(true);

        $firstName = new Input('firstName');
        $firstName->setRequired(false);

        $lastName = new Input('lastName');
        $lastName->setRequired(false);

        $middleName = new Input('middleName');
        $middleName->setRequired(false);

        $this->add($email)
            ->add($phone)
            ->add($firstName)
            ->add($lastName)
            ->add($middleName);
    }
}