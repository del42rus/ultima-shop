<?php

namespace Ultima\Clients\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;

class AddressInputFilter extends InputFilter
{
    public function __construct()
    {
        $address = new Input('address');
        $address->setRequired(true);

        $this->add($address);
    }
}