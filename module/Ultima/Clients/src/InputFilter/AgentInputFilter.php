<?php

namespace Ultima\Clients\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

class AgentInputFilter extends InputFilter
{
    public function __construct()
    {
        $name = new Input('name');
        $name->setRequired(true);

        $inn = new Input('inn');
        $inn->setRequired(true)
            ->getValidatorChain()
            ->attach(new Validator\StringLength(10));

        $kpp = new Input('kpp');
        $kpp->setRequired(false)
            ->getValidatorChain()
            ->attach(new Validator\StringLength(9));

        $bic = new Input('bic');
        $bic->setRequired(false);

        $okpo = new Input('okpo');
        $okpo->setRequired(false);

        $settlementAccount = new Input('settlementAccount');
        $settlementAccount->setRequired(false);

        $bankName = new Input('bankName');
        $bankName->setRequired(false);

        $bankCorrAccount = new Input('bankCorrAccount');
        $bankCorrAccount->setRequired(false);

        $email = new Input('email');
        $email->setRequired(false)
            ->getValidatorChain()
            ->attach(new Validator\EmailAddress());

        $this->add($name)
            ->add($inn)
            ->add($okpo)
            ->add($settlementAccount)
            ->add($settlementAccount)
            ->add($bankName)
            ->add($kpp)
            ->add($email);
    }
}