<?php

namespace Ultima\Core\Listener;

use Ultima\Core\Entity\SwapTable;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Ultima\Core\Service\SwapTableService;

class MappingListener
{
    /**
     * @var SwapTableService
     */
    private $swapTableService;

    public function __construct(SwapTableService $swapTableService)
    {
        $this->swapTableService = $swapTableService;
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        /** @var ClassMetadata $classMetadata */
        $classMetadata = $eventArgs->getClassMetadata();

        if ($classMetadata->getName() == SwapTable::class) {
            return;
        }

        $table['name'] = $this->swapTableService->getActiveTable($classMetadata->getTableName())->getTableName();

        $classMetadata->setPrimaryTable($table);

        $associationMappings = $classMetadata->getAssociationMappings();

        foreach ($associationMappings as $fieldName => $mapping) {
            if (!isset($mapping['joinTable'])) {
                continue;
            }

            $mappedTableName = $mapping['joinTable']['name'];

            $mapping['joinTable']['name'] = $this->swapTableService->getActiveTable($mappedTableName)->getTableName();
            $classMetadata->setAssociationOverride($fieldName, $mapping);
        }
    }
}