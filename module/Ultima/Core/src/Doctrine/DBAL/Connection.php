<?php

namespace Ultima\Core\Doctrine\DBAL;

use Doctrine\DBAL\Cache\QueryCacheProfile;

class Connection extends \Doctrine\DBAL\Connection
{
    private function processSql($sql)
    {
        $matches = [];

        preg_match_all('/#(.*?)#/', $sql, $matches);

        $searches = [];
        $replaces = [];

        foreach ($matches[0] as $search) {
            @list($tableName, $state) = explode(':', trim($search, '#'));
            $state = $state ?? 'active';

            $tableName = $this->getTableName($tableName,$state);

            $searches[] = $search;
            $replaces[] = $tableName;
        }

        if (!empty($searches)) {
            $sql = str_replace($searches, $replaces, $sql);
        }

        return $sql;
    }

    private function getTableName($name, $state)
    {
        return $this->fetchColumn('SELECT `table_name` FROM `swap_tables` WHERE `name` = ? AND `state` = ?', [$name, $state]);
    }

    public function query()
    {
        $args = func_get_args();
        $sql = $this->processSql($args[0]);

        return parent::query($sql);
    }

    public function prepare($statement)
    {
        $statement = $this->processSql($statement);

        return parent::prepare($statement);
    }

    public function executeUpdate($query, array $params = array(), array $types = array())
    {
        $query = $this->processSql($query);

        return parent::executeUpdate($query, $params, $types);
    }

    public function executeQuery($query, array $params = array(), $types = array(), QueryCacheProfile $qcp = null)
    {
        $query = $this->processSql($query);

        return parent::executeQuery($query, $params, $types, $qcp);
    }

    public function fetchAll($sql, array $params = array(), $types = array())
    {
        $sql = $this->processSql($sql);

        return parent::fetchAll($sql, $params, $types);
    }

    public function fetchArray($statement, array $params = array(), array $types = array())
    {
        $statement = $this->processSql($statement);

        return parent::fetchArray($statement, $params, $types);
    }

    public function fetchColumn($statement, array $params = array(), $column = 0, array $types = array())
    {
        $statement = $this->processSql($statement);

        return parent::fetchColumn($statement, $params, $column, $types);
    }

    public function delete($tableExpression, array $identifier, array $types = array())
    {
        $tableExpression = $this->processSql($tableExpression);

        return parent::delete($tableExpression, $identifier, $types);
    }

    public function insert($tableExpression, array $data, array $types = array())
    {
        $tableExpression = $this->processSql($tableExpression);

        $this->connect();

        if (empty($data)) {
            return $this->executeUpdate('INSERT IGNORE INTO ' . $tableExpression . ' ()' . ' VALUES ()');
        }

        $columnList = array();
        $paramPlaceholders = array();
        $paramValues = array();

        foreach ($data as $columnName => $value) {
            $columnList[] = $columnName;
            $paramPlaceholders[] = '?';
            $paramValues[] = $value;
        }

        return $this->executeUpdate(
            'INSERT IGNORE INTO ' . $tableExpression . ' (' . implode(', ', $columnList) . ')' .
            ' VALUES (' . implode(', ', $paramPlaceholders) . ')',
            $paramValues,
            is_string(key($types)) ? $this->extractTypeValues($columnList, $types) : $types
        );
    }

    private function extractTypeValues(array $columnList, array $types)
    {
        $typeValues = array();

        foreach ($columnList as $columnIndex => $columnName) {
            $typeValues[] = isset($types[$columnName])
                ? $types[$columnName]
                : \PDO::PARAM_STR;
        }

        return $typeValues;
    }

    public function update($tableExpression, array $data, array $identifier, array $types = array())
    {
        $tableExpression = $this->processSql($tableExpression);

        return parent::update($tableExpression, $data, $identifier, $types);
    }

    public function exec($statement)
    {
        $statement = $this->processSql($statement);

        return parent::exec($statement);
    }

    public function truncate($tableExpression)
    {
        $dbPlatform = $this->getDatabasePlatform();

        $this->beginTransaction();
        try {
            $query = $dbPlatform->getTruncateTableSql($tableExpression);
            $this->executeUpdate($query);
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
        }
    }

    public function insertSet($tableExpression, array $data, array $types = array())
    {
        $this->transactional(function () use ($tableExpression, $data, $types){
            foreach ($data as $row) {
                $this->insert($tableExpression, $row);
            }
        });
    }
}