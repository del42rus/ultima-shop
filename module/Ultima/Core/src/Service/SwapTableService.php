<?php

namespace Ultima\Core\Service;

use Ultima\Core\Repository\SwapTableRepository;
use Ultima\Core\Entity\SwapTable;

class SwapTableService extends AbstractService
{
    /** @var SwapTableRepository */
    protected $swapTableRepository;

    public function __construct(SwapTableRepository $swapTableRepository)
    {
        $this->swapTableRepository = $swapTableRepository;
    }

    /**
     * @param $name
     * @return SwapTable
     */
    public function getActiveTable($name)
    {
        return $this->swapTableRepository->getActiveTable($name);
    }

    /**
     * @param $name
     * @return SwapTable
     */
    public function getPassiveTable($name)
    {
        return $this->swapTableRepository->getPassiveTable($name);
    }

    /**
     * @param $name
     */
    public function swapTable($name)
    {
        $activeTable = $this->getActiveTable($name);
        $passiveTable = $this->getPassiveTable($name);

        $activeTableName = $activeTable->getTableName();
        $passiveTableName = $passiveTable->getTableName();

        $activeTable->setTableName($passiveTableName);
        $passiveTable->setTableName($activeTableName);

        $this->objectManager->persist($activeTable);
        $this->objectManager->persist($passiveTable);

        $this->objectManager->flush();
    }
}