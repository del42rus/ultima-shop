<?php

namespace Ultima\Core\Service\Initializer;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Initializer\InitializerInterface;

class ObjectManagerInitializer implements InitializerInterface
{
    public function __invoke(ContainerInterface $container, $instance)
    {
        if ($instance instanceof ObjectManagerAwareInterface) {
            /** @var EntityManager $entityManager */
            $entityManager = $container->get('doctrine.entitymanager.orm_default');
            $instance->setObjectManager($entityManager);
        }
    }
}