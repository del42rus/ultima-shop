<?php

namespace Ultima\Core\Service;

use Ultima\Core\Doctrine\DBAL\Connection;
use Doctrine\Common\Persistence\ObjectManagerAware;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;

abstract class AbstractService implements ObjectManagerAwareInterface
{
    use ProvidesObjectManager;

    public function save($entity)
    {
        $this->objectManager->persist($entity);
        $this->objectManager->flush();
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->objectManager->getConnection();
    }
}