<?php

namespace Ultima\Core\Service\Factory;

use Ultima\Core\Entity\SwapTable;
use Ultima\Core\Repository\SwapTableRepository;
use Ultima\Core\Service\SwapTableService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SwapTableServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        /** @var SwapTableRepository $swapTableRepository */
        $swapTableRepository = $entityManager->getRepository(SwapTable::class);

        $swapTableService = new SwapTableService($swapTableRepository);

        return $swapTableService;
    }
}