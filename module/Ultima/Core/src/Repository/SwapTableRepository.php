<?php

namespace Ultima\Core\Repository;

use Ultima\Core\Entity\SwapTable;
use Doctrine\ORM\EntityRepository;

class SwapTableRepository extends EntityRepository
{
    /**
     * @param $name
     * @return SwapTable
     */
    public function getActiveTable($name)
    {
        return $this->findOneBy(['name' => $name, 'state' => 'active']);
    }

    /**
     * @param $name
     * @return SwapTable
     */
    public function getPassiveTable($name)
    {
        return $this->findOneBy(['name' => $name, 'state' => 'passive']);
    }
}