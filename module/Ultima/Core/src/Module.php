<?php

namespace Ultima\Core;

use Ultima\Core\Service\SwapTableService;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $doctrineEntityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $doctrineEventManager = $doctrineEntityManager->getEventManager();
        $swapTableService = $serviceManager->get(SwapTableService::class);

        $doctrineEventManager->addEventListener(
            [\Doctrine\ORM\Events::loadClassMetadata],
            new \Ultima\Core\Listener\MappingListener($swapTableService)
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
