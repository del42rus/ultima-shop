<?php

namespace Ultima\Core\Hydrator\NamingStrategy;

use Zend\Hydrator\NamingStrategy\NamingStrategyInterface;

final class UpperCamelCaseNamingStrategy implements NamingStrategyInterface
{
    /**
     * {@inheritDoc}
     */
    public function hydrate($name)
    {
        return $name;
    }

    /**
     * {@inheritDoc}
     */
    public function extract($name)
    {
        return ucfirst($name);
    }
}
