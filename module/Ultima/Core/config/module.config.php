<?php

namespace Ultima\Core;

use Ultima\Core\Query\QueryManagerAbstractFactory;
use Ultima\Core\Service\Factory\SwapTableServiceFactory;
use Ultima\Core\Service\Initializer\ObjectManagerInitializer;
use Ultima\Core\Service\SwapTableService;

return [
    'service_manager' => [
        'initializers' => [
            ObjectManagerInitializer::class
        ],
        'factories' => [
            SwapTableService::class => SwapTableServiceFactory::class
        ],
    ],
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
];
