<?php

namespace Ultima\Offices;

use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Ultima\Offices\Replicator\LocationsReplicator;
use Ultima\Offices\Replicator\OfficesReplicator;
use Ultima\Offices\Replicator\StoresReplicator;

return [
    'replicators' => [
        'locations' => LocationsReplicator::class,
        'offices' => OfficesReplicator::class,
        'stores' => StoresReplicator::class
    ],
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],

];