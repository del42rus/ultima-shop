<?php

namespace Ultima\Offices\Repository;

use Doctrine\ORM\EntityRepository;

class LocationRepository extends EntityRepository implements LocationRepositoryInterface
{
    public function getLocationById($id)
    {
        return $this->find($id);
    }
}