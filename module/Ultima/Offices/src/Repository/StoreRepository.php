<?php

namespace Ultima\Offices\Repository;
;
use Doctrine\ORM\EntityRepository;

class StoreRepository extends EntityRepository implements LocationRepositoryInterface
{
    public function getStoreById($id)
    {
        return $this->find($id);
    }
}