<?php

namespace Ultima\Offices\Service;

use Ultima\Offices\Repository\LocationRepositoryInterface;

class LocationService
{
    /**
     * @var LocationRepositoryInterface
     */
    private $locationRepository;

    public function __construct(LocationRepositoryInterface $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * @return LocationRepositoryInterface
     */
    public function getCurrentLocation()
    {
        return $this->locationRepository->getLocationById($this->getCurrentLocationId());
    }

    /**
     * @return int
     */
    public function getCurrentLocationId()
    {
        return \Ultima\Offices\Entity\Location::DEFAULT_LOCATION_ID;
    }
}