<?php

namespace Ultima\Offices\Service\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Offices\Entity\Location;
use Ultima\Offices\Repository\LocationRepositoryInterface;
use Ultima\Offices\Service\LocationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class LocationServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /** @var LocationRepositoryInterface $locationRepository */
        $locationRepository = $entityManager->getRepository(Location::class);

        return new LocationService($locationRepository);
    }
}