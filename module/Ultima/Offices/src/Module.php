<?php

namespace Ultima\Offices;

use Ultima\Offices\Service\Factory\LocationServiceFactory;
use Ultima\Offices\Service\LocationService;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'location' => Controller\Plugin\Factory\LocationFactory::class
            ],
        ];
    }

    public function getViewHelperConfig() {
        return [
            'factories' => [
                'location' => View\Helper\Factory\LocationFactory::class
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                LocationService::class => LocationServiceFactory::class,
            ]
        ];
    }
}
