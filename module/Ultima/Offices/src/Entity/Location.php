<?php

namespace Ultima\Offices\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Location
{
    const DEFAULT_LOCATION_ID = 2;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var Location
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var string
     */
    private $name;

    /**
     * @var
     */
    private $zoneId;

    /**
     * @var ArrayCollection
     */
    private $offices;

    /**
     * @var ArrayCollection
     */
    private $stores;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->offices = new ArrayCollection();
        $this->stores = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Location
     */
    public function getParent(): Location
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * @return ArrayCollection
     */
    public function getOffices()
    {
        return $this->offices;
    }

    /**
     * @return array
     */
    public function getOfficeIds()
    {
        $ids = [];

        /** @var Office $office */
        foreach ($this->offices as $office) {
            $ids[] = $office->getId();
        }

        return $ids;
    }

    /**
     * @return ArrayCollection
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * @return array
     */
    public function getStoreIds()
    {
        $ids = [];

        /** @var Store $store */
        foreach ($this->stores as $store) {
            $ids[] = $store->getId();
        }

        return $ids;
    }

}