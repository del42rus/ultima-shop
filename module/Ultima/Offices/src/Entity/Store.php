<?php

namespace Ultima\Offices\Entity;

class Store
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var Office
     */
    private $office;

    /**
     * @var Location
     */
    private $location;

    /**
     * @var bool
     */
    private $isPickup;

    /**
     * @var bool
     */
    private $isDelivery;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @return Location
     */
    public function getLocation(): Location
    {
        return $this->location;
    }

    /**
     * @return bool
     */
    public function isPickup(): bool
    {
        return $this->isPickup;
    }

    /**
     * @return bool
     */
    public function isDelivery(): bool
    {
        return $this->isDelivery;
    }
}