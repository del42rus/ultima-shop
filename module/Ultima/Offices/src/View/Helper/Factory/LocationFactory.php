<?php

namespace Ultima\Offices\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Offices\Service\LocationService;
use Ultima\Offices\View\Helper\Location;
use Zend\ServiceManager\Factory\FactoryInterface;

class LocationFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var LocationService $locationService */
        $locationService = $container->get(LocationService::class);

        return new Location($locationService);
    }
}