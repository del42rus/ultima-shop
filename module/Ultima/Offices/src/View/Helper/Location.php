<?php

namespace Ultima\Offices\View\Helper;

use Ultima\Offices\Repository\LocationRepositoryInterface;
use Ultima\Offices\Service\LocationService;
use Zend\View\Helper\AbstractHelper;

class Location extends AbstractHelper
{
    /**
     * @var LocationService
     */
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function __invoke()
    {
        return $this->locationService->getCurrentLocation();
    }
}