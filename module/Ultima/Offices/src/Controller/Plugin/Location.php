<?php

namespace Ultima\Offices\Controller\Plugin;

use Ultima\Offices\Service\LocationService;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Location extends AbstractPlugin
{
    /**
     * @var LocationService
     */
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function __invoke()
    {
        return $this->locationService->getCurrentLocation();
    }
}