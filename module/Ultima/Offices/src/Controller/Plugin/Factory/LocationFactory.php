<?php

namespace Ultima\Offices\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Offices\Controller\Plugin\Location;
use Ultima\Offices\Service\LocationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class LocationFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var LocationService $locationService */
        $locationService = $container->get(LocationService::class);

        return new Location($locationService);
    }
}