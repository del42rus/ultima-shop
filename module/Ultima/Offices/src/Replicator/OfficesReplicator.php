<?php

namespace Ultima\Offices\Replicator;

use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class OfficesReplicator extends AbstractReplicator
{
    protected $officesTable = '#offices:passive#';

    public function __construct()
    {
        $this->addSwapTable('offices');
    }

    public function process()
    {
       $this->processOffices();
    }

    protected function processOffices()
    {
        $this->logger->info('Insert offices');
        $result = $this->apiClient->get('GetOffices');

        if (empty($result)) {
            $this->removeSwapTable('offices');
            return;
        }

        $this->getConnection()->truncate($this->officesTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'name' => 'Name',
            'location_id' => 'LocationId',
            'address' => 'Address'
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->officesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->officesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Offices inserted = ' . $totalCount);
    }
}