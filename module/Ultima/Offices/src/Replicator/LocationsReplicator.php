<?php

namespace Ultima\Offices\Replicator;

use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class LocationsReplicator extends AbstractReplicator
{
    protected $currenciesTable = '#locations:passive#';

    public function __construct()
    {
        $this->addSwapTable('locations');
    }

    public function process()
    {
       $this->processLocations();
    }

    protected function processLocations()
    {
        $this->logger->info('Insert locations');
        $result = $this->apiClient->get('GetLocations');

        if (empty($result)) {
            $this->removeSwapTable('locations');
            return;
        }

        $this->getConnection()->truncate($this->currenciesTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'parent_id' => 'ParentId',
            'name' => 'Name',
            'zone_id' => 'ZoneId',
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->currenciesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->currenciesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Locations inserted = ' . $totalCount);
    }
}