<?php

namespace Ultima\Offices\Replicator;

use Ultima\Replication\Filter\BooleanToInt;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class StoresReplicator extends AbstractReplicator
{
    protected $storesTable = '#stores:passive#';

    public function __construct()
    {
        $this->addSwapTable('stores');
    }

    public function process()
    {
       $this->processStores();
    }

    protected function processStores()
    {
        $this->logger->info('Insert stores');
        $result = $this->apiClient->get('GetStores');

        if (empty($result)) {
            $this->removeSwapTable('stores');
            return;
        }

        $this->getConnection()->truncate($this->storesTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'office_id' => '%d: OfficeId',
            'location_id' => '%d: LocationId',
        ]);

        $filter = new BooleanToInt();

        $mapper->setMapping('is_delivery', 'IsDelivery', function ($field) use ($filter) {
            return $filter->filter($field);
        });

        $mapper->setMapping('is_pickup', 'IsAutoRetreive', function ($field) use ($filter) {
            return $filter->filter($field);
        });

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->storesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->storesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Stores inserted = ' . $totalCount);
    }
}