<?php

namespace Ultima\Delivery;

use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Ultima\Delivery\Replicator\DeliveryReplicator;

return [
    'replicators' => [
        'delivery' => DeliveryReplicator::class,
    ],
];