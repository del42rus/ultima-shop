<?php

namespace Ultima\Delivery\Replicator;

use Ultima\Replication\Filter\JsonDateToDate;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class DeliveryReplicator extends AbstractReplicator
{
    protected $productDatesTable = '#product_dates:passive#';

    public function __construct()
    {
        $this->addSwapTable('product_dates');
    }

    public function process()
    {
       $this->processProductDates();
    }

    protected function processProductDates()
    {
        $this->logger->info('Insert product dates');
        $result = $this->apiClient->get('GetGoodsRetrieveDays');

        if (empty($result)) {
            $this->removeSwapTable('product_dates');
            return;
        }

        $this->getConnection()->truncate($this->productDatesTable);

        $mapper = new Mapper([
            'product_id' => '%d: ArticleId',
            'location_id' => '%d: LocationId',
        ]);

        $filter = new JsonDateToDate();

        $mapper->setMapping('pickup_date', 'RetrieveDate', function ($field, $row) use ($filter) {
            return $filter->filter($field);
        });
        $mapper->setMapping('delivery_date', 'MinDeliveryDate', function ($field, $row) use ($filter) {
            return $filter->filter($field);
        });
        $mapper->setMapping('suborder_delivery_date', 'MaxDeliveryDate', function ($field, $row) use($filter) {
            return $filter->filter($field);
        });

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productDatesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productDatesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product dates inserted = ' . $totalCount);
    }
}