<?php

namespace Ultima\Replication;

use Ultima\Replication\Console\Command\Factory\ReplicationCommandFactory;
use Ultima\Replication\Console\Command\ReplicationCommand;
use Ultima\Replication\Filter\JsonDateToDate;
use Ultima\Replication\Filter\Translit;
use Ultima\Replication\Filter\Sluggify;
use Ultima\Replication\Logger\Factory\LoggerFactory;
use Ultima\Replication\Replicator\Factory\ReplicatorAbstractFactory;
use Zend\ServiceManager\Factory\InvokableFactory;
use Ultima\Replication\Logger\Logger;

return [
    'replication' => [
        'lock_path' => 'data/lock/'
    ],

    'service_manager' => [
        'abstract_factories' => [
            ReplicatorAbstractFactory::class
        ],
        'factories' => [
            ReplicationCommand::class => ReplicationCommandFactory::class,
            Logger::class => LoggerFactory::class
        ]
    ],

    'console' => [
        'commands' => [
            ReplicationCommand::class
        ]
    ],

    'filters' => [
        'aliases' => [
            'translit' => Translit::class,
            'sluggify' => Sluggify::class,
            'jsonDateToDate' => JsonDateToDate::class
        ],
        'factories' => [
            Translit::class => InvokableFactory::class,
            Sluggify::class => InvokableFactory::class,
            JsonDateToDate::class => InvokableFactory::class
        ]
    ]
];