<?php

namespace Ultima\Replication\Mapper;

use Zend\Filter\FilterPluginManager;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\Exception\InvalidArgumentException;

class Mapper
{
	private $mapHash = [];

    /**
     * @var FilterPluginManager
     */
    protected $filters;

	public function __construct(array $mappings = [])
    {
		if (0 < sizeof($mappings)) {
			$this->setMappings($mappings);
		}
	}

    /**
     * @param $resultField
     * @param $sourceField
     * @param callable|null $converter
     * @return $this
     */
	public function setMapping($resultField, $sourceField, callable $converter = null)
    {
		$resultField = trim($resultField);
		$sourceField = trim($sourceField);
		
		if (empty($resultField)) {
			throw new \InvalidArgumentException('Not specified result field');
		}

		if (empty($sourceField) && (null === $converter)) {
			throw new \InvalidArgumentException('Must set source field or converter function');
		}
		
		$this->mapHash[$resultField] = [
			'sourceField' => $sourceField,
			'resultField' => $resultField,
			'converter' => $converter
		];

		return $this;
	}

    /**
     * @param array $mappings
     * @return $this
     */
	public function setMappings(array $mappings)
    {
		foreach ($mappings as $resultField => $sourceField) {
			$this->setMapping($resultField, $sourceField);
		}

		return $this;
	}

    /**
     * @return $this
     */
	public function clearMappings()
    {
        $this->mapHash = [];
        return $this;
    }

    /**
     * @param $data
     * @return array
     */
	public function convert($data)
    {
		if (!is_array($data) && !$data instanceof \ArrayAccess) {
			$dataType = (is_object($data) ? get_class($data) : gettype($data));
			throw new \InvalidArgumentException(sprintf('Data must be an array or an object implementing \Traversable. Type of data is %s', $dataType));
		}

		$out = [];
		foreach ($this->mapHash as $map) {
			$out[$map['resultField']] = $this->convertField($data, $map);
		}

		return $out;
	}

    /**
     * @param $data
     * @param $map
     * @return string
     */
	private function convertField($data, $map)
    {
		$sourceField = $map['sourceField'];
		$converter = $map['converter'];
		
		$result = '';
		
		if (!empty($sourceField)) {
			if (false !== stripos($sourceField, ':')) {
				$sourceParts = explode(':', $sourceField);
				$expression = trim($sourceParts[0]);
				$fieldName = trim($sourceParts[1]);
				$result = sprintf($expression, $data[$fieldName] ?? null);
			} else {
				$result = $data[$sourceField] ?? null;
			}
		}
		
		if (null !== $converter && isset($data[$sourceField])) {
			$result = $converter($data[$sourceField], $data);
		}
		
		return $result;
	}
}
