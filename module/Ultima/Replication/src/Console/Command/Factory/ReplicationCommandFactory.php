<?php

namespace Ultima\Replication\Console\Command\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Replication\Console\Command\ReplicationCommand;
use Zend\ServiceManager\Factory\FactoryInterface;

class ReplicationCommandFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ReplicationCommand('replicate', $container);
    }
}