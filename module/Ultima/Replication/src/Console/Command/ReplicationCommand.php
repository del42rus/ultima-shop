<?php

namespace Ultima\Replication\Console\Command;

use Ultima\Replication\Replicator\AbstractReplicator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\ServiceManager\ServiceManager;

class ReplicationCommand extends Command
{
    protected $serviceManager;

    public function __construct($name, ServiceManager $serviceManager)
    {
        parent::__construct($name);
        $this->serviceManager = $serviceManager;
    }

    protected function configure()
    {
        $this->addArgument('name', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $replicatorName = $input->getArgument('name');

        /** @var AbstractReplicator $replicator */
        $replicator = $this->serviceManager->get($replicatorName);
        $replicator->run();
    }
}