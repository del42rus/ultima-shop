<?php

namespace Ultima\Replication\Collection;

use InvalidArgumentException;

class NestedSetBuilder
{
	/**
	 * @var array
	 */
	private $nodes = [];

	/**
	 * @var array
	 */
	private $nodeLinks = [];

	/**
	 * @var array
	 */
	private $list = [];

	/**
	 * @param integer $id        	
	 * @param integer $parentId        	
	 * @throws InvalidArgumentException
	 */
	public function addNode($id, $parentId = 0)
    {
		if (!is_integer($id)) {
			throw new InvalidArgumentException('Id must be integer');
		}
		if (0 > $id) {
			throw new InvalidArgumentException('Id must be greater than zero');
		}
		if (!empty($parentId) && !is_integer($parentId)) {
			throw new InvalidArgumentException('Parent id must be integer');
		}
		$this->list[$id] = [
			'id' => $id,
			'parentId' => $parentId 
		];
	}

	/**
	 * Create tree-based structure
	 * 
	 * @return array
	 */
	public function buildTree()
    {
		$this->nodes = array();
		$this->nodeLinks = array();
		foreach ($this->list as $id => $item) {
			if (0 == $item['parentId']) {
				$this->__add($id);
			}
		}
		foreach ($this->nodes as $node) {
			$this->__initLevel($node['id']);
		}
		
		$left = 0;
		foreach ($this->nodes as $node) {
			$this->__initBorders($node['id'], $left);
		}
		return $this->nodes;
	}

	/**
	 * Create list-based structure
	 * 
	 * @return array
	 */
	public function buildList() {
		$this->buildTree();
		$list = array();
		foreach ($this->nodeLinks as &$link)
		{
			$list[$link['id']] = [
				'id' => $link['id'],
				'left' => $link['left'],
				'right' => $link['right'],
				'level' => $link['level'] 
			];
		}
		return $list;
	}

	/**
	 *
	 * @param integer $id        	
	 * @param integer $parentId        	
	 */
	private function __add($id, $parentId = 0)
    {
		$this->nodes[$id] = [
			'id' => $id,
			'parentId' => $parentId,
			'left' => null,
			'right' => null,
			'level' => 0,
			'childs' => [] 
		];
		$this->nodeLinks[$id] = &$this->nodes[$id];
		$this->__addByParentId($id);
	}

	/**
	 *
	 * @param integer $parentId        	
	 */
	private function __addByParentId($parentId)
    {
		$parent = &$this->nodeLinks[$parentId];
		$childs = $this->__enumChilds($parentId);
		foreach ($childs as $childId) {
			$parent['childs'][$childId] = [
				'id' => $childId,
				'parentId' => $parentId,
				'left' => null,
				'right' => null,
				'level' => null,
				'childs' => [] 
			];
			$this->nodeLinks[$childId] = &$parent['childs'][$childId];
			$this->__addByParentId($childId);
		}
	}

	/**
	 *
	 * @param integer $id        	
	 * @return array
	 */
	private function __enumChilds($id)
    {
		$list = array();
		foreach ($this->list as $item) {
			if ($id == $item['parentId']) {
				$list[] = $item['id'];
			}
		}
		return $list;
	}

	/**
	 *
	 * @param integer $id        	
	 * @param integer $level        	
	 */
	private function __initLevel($id, $level = 0)
    {
		$node = &$this->nodeLinks[$id];
		$node['level'] = $level;
		$childs = $this->__enumChilds($id);
		foreach ($childs as $childId) {
			$this->__initLevel($childId, $level + 1);
		}
	}

	/**
	 *
	 * @param integer $id        	
	 * @param integer $left        	
	 */
	private function __initBorders($id, &$left)
    {
		$left += 1;
		$node = &$this->nodeLinks[$id];
		$node['left'] = $left;
		$node['right'] = $left + ($this->__countAllChilds($id) * 2) + 1;
		$childs = $this->__enumChilds($id);
		foreach ($childs as $childId) {
			$this->__initBorders($childId, $left);
		}
		$left = $node['right'];
	}

	/**
	 *
	 * @param integer $id        	
	 * @return integer
	 */
	private function __countAllChilds($id)
    {
		$count = 0;
		$node = &$this->nodeLinks[$id];
		$count += sizeof($node['childs']);
		$childs = $this->__enumChilds($id);
		foreach ($childs as $childId) {
			$count += $this->__countAllChilds($childId);
		}
		return $count;
	}

}