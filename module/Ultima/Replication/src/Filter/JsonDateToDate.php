<?php

namespace Ultima\Replication\Filter;

use Zend\Filter\AbstractFilter;

class JsonDateToDate extends AbstractFilter
{
    public function filter($value)
    {
        preg_match('/(\d{10})(\d{3})([\+\-]\d{4})/', $value, $matches);

        // Get the timestamp as the TS tring / 1000
        $ts = (int) $matches[1];

        // Get the timezone name by offset
        $tz = (int) $matches[3];
        $tz = timezone_name_from_abbr("", $tz / 100 * 3600, false);
        $tz = new \DateTimeZone($tz);

        // Create a new DateTime, set the timestamp and the timezone
        $dt = new \DateTime();
        $dt->setTimestamp($ts);
        $dt->setTimezone($tz);

        return $dt->format('Y-m-d H:i:s');
    }
}