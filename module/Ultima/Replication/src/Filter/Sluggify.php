<?php

namespace Ultima\Replication\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\FilterChain;

class Sluggify extends AbstractFilter
{
    public function filter($value)
    {
        $filterChain = new FilterChain();

        $filterChain->attach(new Translit())
            ->attachByName('stringtolower', ['encoding' => 'utf-8']);

        return $filterChain->filter($value);
    }
}