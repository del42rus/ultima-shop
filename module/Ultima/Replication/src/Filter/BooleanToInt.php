<?php

namespace Ultima\Replication\Filter;

use Zend\Filter\AbstractFilter;

class BooleanToInt extends AbstractFilter
{
    public function filter($value)
    {
        if (is_bool($value)) {
            return $value ? 1 : 0;
        }

        return (0 == strcasecmp('true', $value)) ? 1 : 0;
    }
}