<?php

namespace Ultima\Replication\Replicator\Factory;

use Ultima\Core\Service\SwapTableService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Ultima\Replication\Replicator\AbstractReplicator;
use Ultima\Replication\Replicator\ReplicatorInterface;
use UltimaClient\Client\Client;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Ultima\Replication\Logger\Logger;

class ReplicatorAbstractFactory implements AbstractFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return false != $this->getReplicatorOrFactoryClass($container, $requestedName);
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $replicatorOrFactoryClass = $this->getReplicatorOrFactoryClass($container, $requestedName);
        $replicatorOrFactory = new $replicatorOrFactoryClass();

        if ($replicatorOrFactory instanceof ReplicatorInterface) {
            $replicator = $replicatorOrFactory;
        } else {
            /** @var AbstractReplicator $replicator */
            $replicator = $replicatorOrFactory($container, $requestedName);
            $replicator->setName($requestedName);
        }

        $apiClient = $container->get(Client::class);
        $replicator->setApiClient($apiClient);

        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        $replicator->setEntityManager($entityManager);

        $logger = $container->build(Logger::class, ['log_path' => $replicator->getLogPath() . $requestedName . '.log']);
        $replicator->setLogger($logger);

        $swapTableService = $container->get(SwapTableService::class);
        $replicator->setSwapTableService($swapTableService);

        return $replicator;
    }

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @return bool
     */
    private function getReplicatorOrFactoryClass(ContainerInterface $container, $requestedName)
    {
        $config = $container->get('Config');

        if (!isset($config['replicators'][$requestedName])) {
            return false;
        }

        return $config['replicators'][$requestedName];
    }
}