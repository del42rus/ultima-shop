<?php

namespace Ultima\Replication\Replicator;

interface ReplicatorInterface
{
    public function process();
}