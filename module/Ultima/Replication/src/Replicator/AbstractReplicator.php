<?php

namespace Ultima\Replication\Replicator;

use Ultima\Core\Service\SwapTableService;
use Doctrine\ORM\EntityManager;
use UltimaClient\Client\Client;
use Ultima\Core\Doctrine\DBAL\Connection;
use Zend\Log\Logger;

abstract class AbstractReplicator implements ReplicatorInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $logPath = 'data/log/';

    /**
     * @var string
     */
    protected $lockPath = 'data/lock/';

    /**
     * @var Client
     */
    protected $apiClient;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var array
     */
    protected $swapTables = [];

    /**
     * @var SwapTableService
     */
    protected $swapTableService;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLockPath()
    {
        return $this->lockPath;
    }

    /**
     * @param mixed $lockPath
     */
    public function setLockPath($lockPath)
    {
        $this->lockPath = $lockPath;
    }

    public function run()
    {
        if (!$this->lock()) {
            return;
        }

        $this->process();
        $this->swapTables();
    }

    protected function lock()
    {
        if (!file_exists($this->getLockPath())) {
            mkdir($this->getLockPath());
        }

        $this->lockFile = @fopen($this->getLockPath() . $this->getName() . '.lock', 'w+');

        if (!$this->lockFile) {
            return false;
        }

        @fwrite($this->lockFile, time());

        if (!flock($this->lockFile, LOCK_EX | LOCK_NB)) {
            return false;
        }

        return true;
    }

    protected function unlock()
    {
        flock($this->lockFile, LOCK_UN);
        fclose($this->lockFile);
    }

    /**
     * @return Client
     */
    public function getApiClient(): Client
    {
        return $this->apiClient;
    }

    /**
     * @param Client $apiClient
     */
    public function setApiClient(Client $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->entityManager->getConnection();
    }

    /**
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return $this->logger;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getLogPath(): string
    {
        return $this->logPath;
    }

    /**
     * @param string $logPath
     */
    public function setLogPath(string $logPath)
    {
        $this->logPath = $logPath;
    }
    /**
     * @param SwapTableService $swapTableService
     */
    public function setSwapTableService(SwapTableService $swapTableService)
    {
        $this->swapTableService = $swapTableService;
    }

    /**
     * @param $tableName
     */
    public function addSwapTable($tableName)
    {
        if (!in_array($tableName, $this->swapTables)) {
            $this->swapTables[] = $tableName;
        }
    }

    /**
     * @param array $tableNames
     */
    public function setSwapTables(array $tableNames)
    {
        $this->swapTables = $tableNames;
    }

    /**
     * @param $tableName
     */
    public function removeSwapTable($tableName)
    {
        if (in_array($tableName, $this->swapTables)) {
            $key = array_search($tableName, $this->swapTables);
            unset($this->swapTables[$key]);
        }
    }

    /**
     * @param $tableName
     * @return bool
     */
    public function isExistSwapTable($tableName)
    {
        return in_array($tableName, $this->swapTables);
    }

    /**
     * @return array
     */
    public function getSwapTables()
    {
        return $this->swapTables;
    }

    /**
     *
     */
    protected function swapTables()
    {
        foreach ($this->swapTables as $tableName) {
            $this->swapTableService->swapTable($tableName);
        }
    }
}