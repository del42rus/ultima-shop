<?php

namespace Ultima\Replication\Logger\Factory;

use Interop\Container\ContainerInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoggerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $logger = new Logger();

        $output = new Stream('php://output');
        $logger->addWriter($output);

        try {
            $stream = @fopen($options['log_path'], 'a', false);
            if (!$stream) {
                throw new \RuntimeException('Failed to open stream');
            }
            $file = new Stream($stream);
            $logger->addWriter($file);
        } catch (\Exception $e) {
            $logger->warn('Warning: wrong log file path: ' . $options['log_path']);
        }

        return $logger;
    }
}