<?php

namespace Ultima\Finance\Replicator;

use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class CurrenciesReplicator extends AbstractReplicator
{
    protected $currenciesTable = '#currencies:passive#';

    public function __construct()
    {
        $this->addSwapTable('currencies');
    }

    public function process()
    {
       $this->processCurrencies();
    }

    protected function processCurrencies()
    {
        $this->logger->info('Insert currencies');
        $result = $this->apiClient->get('GetCurrencies');

        if (empty($result)) {
            $this->removeSwapTable('currencies');
            return;
        }

        $this->getConnection()->truncate($this->currenciesTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'name' => 'Name',
            'main_currency' => '%d: MainCurrency',
            'rate' => '%f: Rate',
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->currenciesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->currenciesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Currencies inserted = ' . $totalCount);
    }
}