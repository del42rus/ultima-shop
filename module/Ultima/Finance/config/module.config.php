<?php

namespace Ultima\Finance;

use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Ultima\Finance\Replicator\CurrenciesReplicator;

return [
    'replicators' => [
        'currencies' => CurrenciesReplicator::class,
    ],
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
];