<?php

namespace Ultima\Order;

use Ultima\Order\Service\CartService;
use Ultima\Order\Service\Factory\CartServiceFactory;
use Ultima\Order\View\Helper\Cart;
use Ultima\Order\View\Helper\Factory\CartFactory;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                CartService::class => CartServiceFactory::class
            ]
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'cart' => CartFactory::class
            ]
        ];
    }
}
