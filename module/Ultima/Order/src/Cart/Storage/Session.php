<?php

namespace Ultima\Order\Cart\Storage;

use Ultima\Order\Entity\CartItem;
use Zend\Session\Container;

/**
 * @property array|mixed items
 */
class Session extends Container implements StorageInterface
{
    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->items = $this->items ?? [];
    }

    public function addItem(CartItem $item)
    {
        $this->items[$item->getId()] = $item;
    }

    public function removeItem($item)
    {
        $itemId = $item instanceof CartItem ? $item->getId() : $item;

        if (!array_key_exists($itemId, $this->items)) {
            return false;
        }

        unset($this->items[$itemId]);

        return true;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function isEmpty()
    {
        return empty($this->items);
    }
}