<?php

namespace Ultima\Order\View\Helper;

use Ultima\Order\Service\CartService;
use Zend\View\Helper\AbstractHelper;

class Cart extends AbstractHelper
{
    /**
     * @var CartService
     */
    private $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function __invoke()
    {
        return $this;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->cartService->getTotal();
    }

    /**
     * @return int
     */
    public function getItemsCount()
    {
        return $this->cartService->getItemsCount();
    }

    /**
     * @param $product
     * @return bool
     */
    public function hasProduct($product)
    {
        return $this->cartService->hasProduct($product);
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->cartService->isEmpty();
    }
}