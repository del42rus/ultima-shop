<?php

namespace Ultima\Order\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Order\Service\CartService;
use Ultima\Order\View\Helper\Cart;
use Zend\ServiceManager\Factory\FactoryInterface;

class CartFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var CartService $cartService */
        $cartService = $container->get(CartService::class);

        return new Cart($cartService);
    }
}