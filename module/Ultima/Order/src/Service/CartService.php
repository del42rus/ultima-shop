<?php

namespace Ultima\Order\Service;

use Ultima\Catalog\Entity\Product;
use Ultima\Order\Cart\Storage\StorageInterface;
use Ultima\Order\Entity\CartItem;

class CartService
{
    /**
     * @var StorageInterface
     */
    private $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return bool
     */
    public function addProduct(Product $product, $quantity = 1)
    {
        if ($quantity < 0) {
            return false;
        }

        if ($quantity == 0) {
            $this->removeProduct($product);
            return false;
        }

        $cartItem = new CartItem();

        $cartItem->setId($product->getId());
        $cartItem->setName($product->getName());
        $cartItem->setPrice($product->getPrice()->getValue());
        $cartItem->setQuantity($quantity);

        $this->storage->addItem($cartItem);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function removeProduct($product)
    {
        $productId = $product instanceof Product ? $product->getId() : $product;

        return $this->storage->removeItem($productId);
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->storage->getItems();
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        $total = 0;
        /** @var CartItem $item */
        foreach ($this->getItems() as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getItemsCount()
    {
        $quantity = 0;
        /** @var CartItem $item */
        foreach ($this->getItems() as $item) {
            $quantity += $item->getQuantity();
        }

        return $quantity;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->storage->isEmpty();
    }

    /**
     * @param $product
     * @return bool
     */
    public function hasProduct($product)
    {
        $productId = $product instanceof Product ? $product->getId() : $product;

        foreach ($this->getItems() as $item) {
            if ($item->getId() == $productId) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $prices
     */
    public function updateItemsPrices(array $prices)
    {
        /** @var CartItem $item */
        foreach ($this->getItems() as $item) {
            if (!isset($prices[$item->getId()])) {
                continue;
            }

            $item->setPrice($prices[$item->getId()]);
        }
    }
}