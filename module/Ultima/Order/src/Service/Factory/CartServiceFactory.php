<?php

namespace Ultima\Order\Service\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Order\Service\CartService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Ultima\Order\Cart\Storage\Session as Storage;

class CartServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $storage = new Storage();

        return new CartService($storage);
    }
}