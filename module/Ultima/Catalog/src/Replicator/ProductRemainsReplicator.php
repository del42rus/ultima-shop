<?php

namespace Ultima\Catalog\Replicator;

use Redis\Client\ProvidesRedisClient;
use Ultima\Replication\Filter\JsonDateToDate;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class ProductRemainsReplicator extends AbstractReplicator
{
    use ProvidesRedisClient;

    private $productRemainsTable = '#product_remains:passive#';

    private $productArrivalsTable = '#product_arrivals:passive#';

    public function __construct()
    {
        $this->addSwapTable('product_remains');
        $this->addSwapTable('product_arrivals');
    }

    public function process()
    {
        $this->processProductRemains();
        $this->processArrivalProducts();
    }

    public function processProductRemains()
    {
        $this->logger->info('Started insert product remains');

        $result = $this->apiClient->get('RedisGetProductRemains');

        if (empty($result)) {
            $this->removeSwapTable('product_remains');
            return;
        }

        $this->getConnection()->truncate($this->productRemainsTable);

        $totalCount = 0;
        $dataSet = [];

        $keys = $result->Keys;

        if (count($keys) > 0) {
            $mapper = new Mapper([
                'product_id' => '%d: ProductId',
                'store_id' => '%d: StoreId',
                'remains_quantity' => '%d: RemainsQuantity',
                'reserve_quantity' => '%d: ReserveQuantity',
                'sell_speed' => '%f: SellSpeed',
            ]);

            foreach ($keys as $key) {
                $pack = json_decode($this->getRedisClient()->get($key));

                foreach ($pack as $item) {
                    $dataSet[] = $mapper->convert((array) $item);
                    if (500 == sizeof($dataSet)) {
                        $this->getConnection()->insertSet($this->productRemainsTable, $dataSet);
                        $totalCount += count($dataSet);
                        $dataSet = [];
                    }
                }

                $this->getRedisClient()->del($key);
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productRemainsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product remains inserted = ' . $totalCount);
    }

    public function processArrivalProducts()
    {
        $this->logger->info('Insert arrival products');
        $result = $this->apiClient->get('GetSupplierRemains');

        if (empty($result)) {
            $this->removeSwapTable('product_arrivals');
            return;
        }

        $this->getConnection()->truncate($this->productArrivalsTable);

        $mapper = new Mapper([
            'product_id' => '%d: ArticleId',
            'office_id' => '%d: OfficeId',
            'quantity' => '%d: Quantity',
        ]);

        $mapper->setMapping('arrival_date', 'ArrivalDate', function ($field, $row) {
            $filter = new JsonDateToDate();
            return $filter->filter($field);
        });

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productArrivalsTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productArrivalsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Arrival products inserted = ' . $totalCount);
    }
}