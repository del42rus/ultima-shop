<?php

namespace Ultima\Catalog\Replicator;

use Redis\Client\ProvidesRedisClient;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class PricesReplicator extends AbstractReplicator
{
    use ProvidesRedisClient;

    private $productPricesTable = '#product_prices:passive#';

    public function __construct()
    {
        $this->addSwapTable('product_prices');
    }

    public function process()
    {
        $this->processProductPrices();
    }

    public function processProductPrices()
    {
        $this->logger->info('Started insert product prices');

        $result = $this->apiClient->get('RedisGetProductPrices');

        if (empty($result)) {
            $this->removeSwapTable('product_prices');
            return;
        }

        $this->getConnection()->truncate($this->productPricesTable);

        $totalCount = 0;
        $dataSet = [];

        $keys = $result->Keys;

        if (count($keys) > 0) {
            $mapper = new Mapper([
                'product_id' => '%d: ProductId',
                'price_category_id' => '%d: CategoryId',
                'zone_id' => '%d: ZoneId',
                'value' => '%f: Value',
                'prev_value' => '%f: PrevValue',
            ]);

            foreach ($keys as $key) {
                $pack = json_decode($this->getRedisClient()->get($key));

                foreach ($pack as $item) {
                    $dataSet[] = $mapper->convert((array) $item);
                    if (500 == sizeof($dataSet)) {
                        $this->getConnection()->insertSet($this->productPricesTable, $dataSet);
                        $totalCount += count($dataSet);
                        $dataSet = [];
                    }
                }

                $this->getRedisClient()->del($key);
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productPricesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product prices inserted = ' . $totalCount);
    }
}