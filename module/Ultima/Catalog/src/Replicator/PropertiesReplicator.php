<?php

namespace Ultima\Catalog\Replicator;

use Ultima\Catalog\Repository\PropertyRepositoryInterface;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class PropertiesReplicator extends AbstractReplicator
{
    private $productPropertiesTable = '#product_properties:passive#';
    private $productPropertyGroupsTable = '#product_property_groups:passive#';
    private $productPropertyUnitsTable = '#product_property_units:passive#';
    private $productPropertyValuesTable = '#product_property_values:passive#';
    private $productPropertiesToTemplatesTable = '#product_properties_to_templates:passive#';
    private $productTemplatesToNativeCategoriesTable = '#product_templates_to_native_categories:passive#';
    private $productsToProductPropertiesTable = '#products_to_product_properties:passive#';

    /**
     * @var PropertyRepositoryInterface
     */
    private $propertyRepository;

    public function __construct(PropertyRepositoryInterface $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;

        $this->setSwapTables([
            'product_properties',
            'product_property_groups',
            'product_property_units',
            'product_property_values',
            'product_properties_to_templates',
            'product_templates_to_native_categories',
            'products_to_product_properties'
        ]);
    }

    public function process()
    {
        $this->processProductProperties();
        $this->processProductPropertyGroups();
        $this->processProductPropertyUnits();
        $this->processProductPropertyValues();
        $this->processProductPropertiesToTemplates();
        $this->processProductTemplatesToNativeCategories();
        $this->processProductsToProductProperties();
    }

    private function processProductProperties()
    {
        $this->logger->info('Started insert product properties');

        $result = $this->apiClient->get('GetProductProperties');

        if (empty($result)) {
            $this->removeSwapTable('product_properties');
            return;
        }

        $this->getConnection()->truncate($this->productPropertiesTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'parent_id' => 'ParentId',
            'name' => 'Name',
            'type_id' => '%d: TypeId',
            'kind_id' => '%d: KindId',
            'unit_id' => 'UnitId',
            'hidden' => '%d: IsHidden',
            'sort_values' => '%d: SortValues'
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productPropertiesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productPropertiesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product properties inserted = ' . $totalCount);
    }

    private function processProductPropertyGroups()
    {
        $this->logger->info('Started insert product property groups');

        $result = $this->apiClient->get('GetProductPropertyGroups');

        if (empty($result)) {
            $this->removeSwapTable('product_property_groups');
            return;
        }

        $this->getConnection()->truncate($this->productPropertyGroupsTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'name' => 'Name',
            'sort_index' => '%d: SortIndex'
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productPropertyGroupsTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productPropertyGroupsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product property groups inserted = ' . $totalCount);
    }

    private function processProductPropertyUnits()
    {
        $this->logger->info('Started insert product property units');

        $result = $this->apiClient->get('GetProductPropertyUnits');

        if (empty($result)) {
            $this->removeSwapTable('product_property_units');
            return;
        }

        $this->getConnection()->truncate($this->productPropertyUnitsTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'name' => 'Name',
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productPropertyUnitsTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productPropertyUnitsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product property units inserted = ' . $totalCount);
    }

    private function processProductPropertyValues()
    {
        $this->logger->info('Started insert product property values');

        $result = $this->apiClient->get('GetProductPropertyValues');

        if (empty($result)) {
            $this->removeSwapTable('product_property_values');
            return;
        }

        $this->getConnection()->truncate($this->productPropertyValuesTable);

        $mapper = new Mapper([
            'id' => '%d: Id',
            'parent_id' => 'ParentId',
            'property_id' => '%d: PropertyId',
            'value' => 'Value',
            'sort_index' => '%d: SortIndex',
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productPropertyValuesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productPropertyValuesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product property values inserted = ' . $totalCount);
    }

    private function processProductPropertiesToTemplates()
    {
        $this->logger->info('Started insert product properties and templates relations');

        $result = $this->apiClient->get('GetProductPropertiesToTemplates');

        if (empty($result)) {
            $this->removeSwapTable('product_property_values');
            return;
        }

        $this->getConnection()->truncate($this->productPropertiesToTemplatesTable);

        $mapper = new Mapper([
            'template_id' => '%d: TemplateId',
            'property_id' => '%d: PropertyId',
            'property_group_id' => '%d: PropertyGroupId',
            'property_sort_index' => '%d: PropertySortIndex',
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productPropertiesToTemplatesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productPropertiesToTemplatesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product properties and templates relations inserted = ' . $totalCount);
    }

    private function processProductTemplatesToNativeCategories()
    {
        $this->logger->info('Started insert product templates and native categories relations');

        $result = $this->apiClient->get('GetProductTemplatesToNativeCategories');

        if (empty($result)) {
            $this->removeSwapTable('product_templates_to_native_categories');
            return;
        }

        $this->getConnection()->truncate($this->productTemplatesToNativeCategoriesTable);

        $mapper = new Mapper([
            'template_id' => '%d: ProductTemplateId',
            'category_id' => '%d: CategoryId'
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productTemplatesToNativeCategoriesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productTemplatesToNativeCategoriesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Product templates and native categories relations inserted = ' . $totalCount);
    }

    private function processProductsToProductProperties()
    {
        $this->logger->info('Started insert property values for products');

        $result = $this->apiClient->get('GetProductsToProductProperties');

        if (empty($result)) {
            $this->removeSwapTable('products_to_product_properties');
            return;
        }

        $this->getConnection()->truncate($this->productsToProductPropertiesTable);

        $mapper = new Mapper([
            'product_id' => '%d: ProductId',
            'property_id' => '%d: PropertyId',
            'property_value_id' => '%d: ValueId',
            'value_number' => '%f: NumberValue',
            'value_boolean' => '%d: BooleanValue',
            'value_string' => 'StringValue',
            'value_text' => 'TextValue'
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->productsToProductPropertiesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productsToProductPropertiesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Property values for products inserted = ' . $totalCount);
    }
}