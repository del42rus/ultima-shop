<?php

namespace Ultima\Catalog\Replicator;

use Redis\Client\ProvidesRedisClient;
use Ultima\Replication\Filter\JsonDateToDate as JsonDateToDateFilter;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class ProductsReplicator extends AbstractReplicator
{
    use ProvidesRedisClient;

    private $productsTable = '#products:passive#';

    public function __construct()
    {
        $this->addSwapTable('products');
    }

    public function process()
    {
        $this->processProducts();
    }

    private function getProductsMapper()
    {
        $mapper = new Mapper([
            'id' => '%d: Id',
            'category_id' => '%d: CategoryId',
            'brand_id' => '%d: BrandId',
            'original_product_id' => '%d: OriginalProductId',
            'name' => 'Name',
            'description' => 'Description',
            'weight' => '%f: Weight',
            'volume' => '%f: Volume',
            'warranty_period' => 'WarrantyPeriod',
            'warranty_period_unit_id' => '%d: WarrantyPeriodUnitId',
        ]);

        $filter = new JsonDateToDateFilter();
        $mapper->setMapping('created_at', 'CreationDate', function ($value) use ($filter) {
            return $filter->filter($value);
        });

        return $mapper;
    }

    public function processProducts()
    {
        $this->logger->info('Started insert products');

        $result = $this->apiClient->get('RedisGetProducts');

        if (empty($result)) {
            $this->removeSwapTable('products');
            return;
        }

        $this->getConnection()->truncate($this->productsTable);

        $totalCount = 0;
        $dataSet = [];

        $keys = $result->Keys;

        if (count($keys) > 0) {
            $mapper = $this->getProductsMapper();

            foreach ($keys as $key) {
                $pack = json_decode($this->getRedisClient()->get($key));

                foreach ($pack as $item) {
                    $dataSet[] = $mapper->convert((array) $item);
                    if (500 == sizeof($dataSet)) {
                        $this->getConnection()->insertSet($this->productsTable, $dataSet);
                        $totalCount += count($dataSet);
                        $dataSet = [];
                    }
                }

                $this->getRedisClient()->del($key);
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->productsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Products inserted = ' . $totalCount);
    }
}