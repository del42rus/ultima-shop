<?php

namespace Ultima\Catalog\Replicator;

use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class BrandsReplicator extends AbstractReplicator
{
    protected $brandsTable = '#brands:passive#';

    public function __construct()
    {
        $this->addSwapTable('brands');
    }

    public function process()
    {
       $this->processBrands();
    }

    protected function processBrands()
    {
        $this->logger->info('Insert brands');
        $result = $this->apiClient->get('GetBrands');

        if (empty($result)) {
            $this->removeSwapTable('brands');
            return;
        }

        $this->getConnection()->truncate($this->brandsTable);

        $mapper = new Mapper(['id' => '%d: Id', 'name' => 'Name']);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->brandsTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->brandsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Brands inserted = ' . $totalCount);
    }
}