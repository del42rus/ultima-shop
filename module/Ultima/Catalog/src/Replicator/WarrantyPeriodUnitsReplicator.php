<?php

namespace Ultima\Catalog\Replicator;

use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class WarrantyPeriodUnitsReplicator extends AbstractReplicator
{
    protected $warrantyPeriodUnitsTable = '#warranty_period_units:passive#';

    public function __construct()
    {
        $this->addSwapTable('warranty_period_units');
    }

    public function process()
    {
        $this->processWarrantyPeriodUnits();
    }

    public function processWarrantyPeriodUnits()
    {
        $this->logger->info('Started insert warranty period units');

        $result = $this->apiClient->get('GetWarrantyPeriodUnits');

        if (empty($result)) {
            $this->removeSwapTable('warranty_period_units');
            return;
        }

        $this->getConnection()->truncate($this->warrantyPeriodUnitsTable);

        $mapper = new Mapper(['id' => '%d: Id', 'name' => 'Name']);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->warrantyPeriodUnitsTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->warrantyPeriodUnitsTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Warranty period units inserted = ' . $totalCount);
    }
}