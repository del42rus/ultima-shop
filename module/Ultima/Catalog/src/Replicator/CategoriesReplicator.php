<?php

namespace Ultima\Catalog\Replicator;

use Ultima\Catalog\Repository\SiteCategoryRepositoryInterface;
use Ultima\Replication\Collection\NestedSetBuilder;
use Ultima\Replication\Mapper\Mapper;
use Ultima\Replication\Replicator\AbstractReplicator;

class CategoriesReplicator extends AbstractReplicator
{
    protected $nativeCategoriesTable = '#native_categories:passive#';

    protected $siteCategoriesTable = '#site_categories:passive#';

    protected $nativeToSiteCategoriesTable = '#native_to_site_categories:passive#';

    public function __construct()
    {
        $this->setSwapTables(['native_categories', 'site_categories', 'native_to_site_categories']);
    }

    public function process()
    {
        $this->processNativeCategories();
        $this->processSiteCategories();
        $this->processNativeToSiteCategories();
    }

    public function processNativeCategories()
    {
        $this->logger->info('Started insert native categories');

        $result = $this->apiClient->get('GetNativeCategories');

        if (empty($result)) {
            $this->removeSwapTable('native_categories');
            return;
        }

        $this->getConnection()->truncate($this->nativeCategoriesTable);

        $mapper = new Mapper(['id' => '%d: Id', 'name' => 'Name', 'parent_id' => 'ParentId']);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->nativeCategoriesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->nativeCategoriesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Native categories inserted = ' . $totalCount);
    }

    public function processSiteCategories()
    {
        $this->logger->info('Started insert site categories');

        $result = $this->apiClient->get('GetSiteCategories');

        if (empty($result)) {
            $this->removeSwapTable('site_categories');
            return;
        }

        $this->getConnection()->truncate($this->siteCategoriesTable);

        $mapper = new Mapper();
        $mapper->setMappings([
            'id' => '%d: Id',
            'original_id' => 'OriginalId',
            'parent_id' => 'ParentId',
            'name' => 'Name',
            'url_name' => 'UrlName',
            'buy_name' => 'BuyName',
            'sort_index' => '%d: SortIndex'
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        $nestedSetBuilder = new NestedSetBuilder();

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->siteCategoriesTable, $dataSet);
                $dataSet = [];
            }

            $nestedSetBuilder->addNode(intval($row->Id), isset($row->ParentId) ? intval($row->ParentId) : 0);
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->siteCategoriesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $list = $nestedSetBuilder->buildList();
        foreach ($list as $item) {
            $this->getConnection()->update($this->siteCategoriesTable,
                ['level' => $item['level'], 'left_key' => $item['left'], 'right_key' => $item['right']],
                ['id' => $item['id']]
            );
        }

        $this->logger->info('Site categories inserted = ' . $totalCount);
    }

    public function processNativeToSiteCategories()
    {
        $this->logger->info('Started insert native and site categories relations');

        $result = $this->apiClient->get('GetNativeToSiteCategories');

        if (empty($result)) {
            $this->removeSwapTable('native_to_site_categories');
            return;
        }

        $this->getConnection()->truncate($this->nativeToSiteCategoriesTable);

        $mapper = new Mapper();
        $mapper->setMappings([
            'category_id' => '%d: CategoryId',
            'site_category_id' => '%d: SiteCategoryId',
        ]);

        $dataSet = [];
        $totalCount = 0;
        $bunchSize = 500;

        foreach ($result as $row) {
            $dataSet[] = $mapper->convert((array) $row);

            if (count($dataSet) == $bunchSize) {
                $totalCount += $bunchSize;
                $this->getConnection()->insertSet($this->nativeToSiteCategoriesTable, $dataSet);
                $dataSet = [];
            }
        }

        if (count($dataSet) > 0) {
            $this->getConnection()->insertSet($this->nativeToSiteCategoriesTable, $dataSet);
            $totalCount += count($dataSet);
        }

        $this->logger->info('Native and site categories relations inserted = ' . $totalCount);
    }
}