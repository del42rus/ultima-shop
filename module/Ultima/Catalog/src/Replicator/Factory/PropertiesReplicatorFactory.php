<?php

namespace Ultima\Catalog\Replicator\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Replicator\PropertiesReplicator;
use Ultima\Catalog\Repository\PropertyRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class PropertiesReplicatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $propertyRepository = new PropertyRepository($entityManager);

        return new PropertiesReplicator($propertyRepository);
    }
}