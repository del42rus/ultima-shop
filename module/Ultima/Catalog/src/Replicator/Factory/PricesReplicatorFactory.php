<?php

namespace Ultima\Catalog\Replicator\Factory;

use Ultima\Catalog\Replicator\PricesReplicator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Predis\Client as RedisClient;

class PricesReplicatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $replicator = new PricesReplicator();

        /** @var RedisClient $redisClient */
        $redisClient = $container->get(RedisClient::class);
        $replicator->setRedisClient($redisClient);

        return $replicator;
    }
}