<?php

namespace Ultima\Catalog\Replicator\Factory;

use Ultima\Catalog\Replicator\ProductsReplicator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Predis\Client as RedisClient;

class ProductsReplicatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $replicator = new ProductsReplicator();

        /** @var RedisClient $redisClient */
        $redisClient = $container->get(RedisClient::class);
        $replicator->setRedisClient($redisClient);

        return $replicator;
    }
}