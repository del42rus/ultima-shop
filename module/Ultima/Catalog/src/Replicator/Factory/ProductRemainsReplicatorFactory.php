<?php

namespace Ultima\Catalog\Replicator\Factory;

use Ultima\Catalog\Replicator\ProductRemainsReplicator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Predis\Client as RedisClient;

class ProductRemainsReplicatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $replicator = new ProductRemainsReplicator();

        /** @var RedisClient $redisClient */
        $redisClient = $container->get(RedisClient::class);
        $replicator->setRedisClient($redisClient);

        return $replicator;
    }
}