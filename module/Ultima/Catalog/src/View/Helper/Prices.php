<?php

namespace Ultima\Catalog\View\Helper;

use Ultima\Catalog\Service\PricesService;
use Zend\View\Helper\AbstractHelper;

class Prices extends AbstractHelper
{
    /**
     * @var PricesService
     */
    private $pricesService;

    public function __construct(PricesService $pricesService)
    {
        $this->pricesService = $pricesService;
    }

    /**
     * @return PricesService
     */
    public function __invoke()
    {
        return $this->pricesService;
    }
}