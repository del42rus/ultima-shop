<?php

namespace Ultima\Catalog\View\Helper;

use Ultima\Catalog\Service\ComparisonService;
use Zend\View\Helper\AbstractHelper;

class Comparison extends AbstractHelper
{
    /**
     * @var ComparisonService
     */
    private $comparisonService;

    public function __construct(ComparisonService $comparisonService)
    {
        $this->comparisonService = $comparisonService;
    }

    public function __invoke()
    {
        return $this->comparisonService;
    }
}