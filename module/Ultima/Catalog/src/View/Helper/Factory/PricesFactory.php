<?php

namespace Ultima\Catalog\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Catalog\Service\PricesService;
use Ultima\Catalog\View\Helper\Prices;
use Zend\ServiceManager\Factory\FactoryInterface;

class PricesFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $pricesService = $container->get(PricesService::class);
        
        return new Prices($pricesService);
    }
}