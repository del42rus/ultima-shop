<?php

namespace Ultima\Catalog\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Catalog\Service\ComparisonService;
use Ultima\Catalog\View\Helper\Comparison;
use Zend\ServiceManager\Factory\FactoryInterface;

class ComparisonFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ComparisonService $compareService */
        $compareService = $container->get(ComparisonService::class);
        
        return new Comparison($compareService);
    }
}