<?php

namespace Ultima\Catalog;

use Ultima\Catalog\Navigation\Service\CatalogNavigationFactory;
use Ultima\Catalog\Service\CatalogService;
use Ultima\Catalog\Service\ComparisonService;
use Ultima\Catalog\Service\Factory\CatalogServiceFactory;
use Ultima\Catalog\Service\Factory\ComparisonServiceFactory;
use Ultima\Catalog\Service\FiltersService;
use Ultima\Catalog\Service\Factory\FiltersServiceFactory;
use Ultima\Catalog\Service\PricesService;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ServiceManager\Factory\InvokableFactory;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'catalog_navigation' => CatalogNavigationFactory::class,
                CatalogService::class => CatalogServiceFactory::class,
                PricesService::class => InvokableFactory::class,
                FiltersService::class => FiltersServiceFactory::class,
                ComparisonService::class => ComparisonServiceFactory::class
            ]
        ];
    }

    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'prices' => Controller\Plugin\Factory\PricesFactory::class
            ],
        ];
    }

    public function getViewHelperConfig() {
        return [
            'factories' => [
                'prices' => View\Helper\Factory\PricesFactory::class,
                'comparison' => View\Helper\Factory\ComparisonFactory::class
            ],
        ];
    }
}
