<?php

namespace Ultima\Catalog\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Rb\Specification\Doctrine\SpecificationRepositoryTrait;
use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Entity\SiteCategory;
use Ultima\Catalog\Repository\Specification\ProductAvailability;

class SiteCategoryRepository extends EntityRepository implements SiteCategoryRepositoryInterface
{
    use SpecificationRepositoryTrait;
    
    /**
     * @return array
     */
    public function getAllCategories()
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $query = $queryBuilder
            ->select('sc')
            ->from(SiteCategory::class, 'sc')
            ->orderBy('sc.leftKey')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @param $categoryId
     * @return null|object
     */
    public function getCategoryById($categoryId)
    {
        return $this->_em->getRepository(SiteCategory::class)->find($categoryId);
    }

    /**
     * @param null $parentId
     * @return array
     */
    public function getCategoriesByParentId($parentId = null)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('sc')
            ->from(SiteCategory::class, 'sc');

        if (null == $parentId) {
            $queryBuilder->where($queryBuilder->expr()->isNull('sc.parent'));
        } else {
            $queryBuilder->where($queryBuilder->expr()->eq('sc.parent', '?1'))
                ->setParameter(1, $parentId);
        }

        $queryBuilder->orderBy('sc.sortIndex');

        $query = $queryBuilder->getQuery()->useQueryCache(true);

        return $query->getResult();
    }

    /**
     * @param $locationId
     * @param $priceCategoryId
     * @return array
     */
    public function getAvailableCategoryIds($locationId, $priceCategoryId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('DISTINCT sc.id')
            ->from(SiteCategory::class, 'sc')
            ->innerJoin('sc.nativeCategories', 'c')
            ->innerJoin(Product::class, 'p', Join::WITH, 'c.id = p.category');

        $this->modifyQueryBuilder($queryBuilder, new ProductAvailability($locationId, $priceCategoryId));

        $query = $queryBuilder
            ->getQuery()
            ->useResultCache(true)
            ->useQueryCache(true);

        return $query->getResult('FetchColumnHydrator');
    }
}