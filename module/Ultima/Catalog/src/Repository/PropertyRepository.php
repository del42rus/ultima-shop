<?php

namespace Ultima\Catalog\Repository;

use Doctrine\DBAL\Cache\QueryCacheProfile;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Ultima\Catalog\Entity\Property;

class PropertyRepository extends EntityRepository implements PropertyRepositoryInterface
{
    /**
     * @param $productIds
     * @return array
     */
    public function getProductsFiltersValues($productIds)
    {
        $connection = $this->_em->getConnection();
        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder
            ->select('ptp.*')
            ->from('#products_to_product_properties#', 'ptp')
            ->innerJoin('ptp', '#product_properties#', 'p', 'ptp.property_id = p.id')
            ->where($queryBuilder->expr()->in('ptp.product_id', $productIds))
            ->andWhere($queryBuilder->expr()->in('p.kind_id', [Property::PROPERTY_KIND_FILTER, Property::PROPERTY_KIND_FILTER4]));

        $stmt = $connection->executeCacheQuery($queryBuilder->getSQL(), [], [], new QueryCacheProfile(0, __METHOD__ . md5(join(',', $productIds))));
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * @param array $propertiesIds
     * @param array|null $valuesIds
     * @return array
     */
    public function getPropertiesByIds(array $propertiesIds, array $valuesIds = null)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('p, pv, pu')
            ->from(Property::class, 'p')
            ->leftJoin('p.unit', 'pu');

        if (!empty($valuesIds)) {
            $queryBuilder->leftJoin('p.values', 'pv', Join::WITH, $queryBuilder->expr()->in('pv.id', $valuesIds));
        } else {
            $queryBuilder->leftJoin('p.values', 'pv');
        }

        $queryBuilder->where($queryBuilder->expr()->in('p.id', $propertiesIds));

        $query = $queryBuilder
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true);

        return $query->getResult();
    }

    /**
     * @param $productIds
     * @return array
     */
    public function getProductsPropertiesValues($productIds)
    {
        $connection = $this->_em->getConnection();
        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder
            ->select('ptp.*')
            ->from('#products_to_product_properties#', 'ptp')
            ->innerJoin('ptp', '#product_properties#', 'p', 'ptp.property_id = p.id')
            ->where($queryBuilder->expr()->in('ptp.product_id', $productIds))
            ->andWhere($queryBuilder->expr()->in('p.kind_id', [Property::PROPERTY_KIND_REGULAR, Property::PROPERTY_KIND_FILTER]));

        $stmt = $connection->executeCacheQuery($queryBuilder->getSQL(), [], [], new QueryCacheProfile(0, __METHOD__ . md5(join(',', $productIds))));
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }
}