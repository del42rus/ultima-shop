<?php

namespace Ultima\Catalog\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Ultima\Catalog\Entity\Category;
use Ultima\Catalog\Entity\Product;

class NativeCategoryRepository implements NativeCategoryRepositoryInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}