<?php

namespace Ultima\Catalog\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Rb\Specification\Doctrine\SpecificationRepositoryTrait;
use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Repository\Specification\ProductAvailability;
use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class ProductRepository extends EntityRepository implements ProductRepositoryInterface
{
    use SpecificationRepositoryTrait;

    /**
     * @param $id
     * @return null|object|Product
     */
    public function getProductById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $productId
     * @param $locationId
     * @param $priceCategoryId
     * @return null|object|Product
     */
    public function getAvailableProductById($productId, $locationId, $priceCategoryId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('p, pr, pa, pd, pp')
            ->from(Product::class, 'p');

        $this->modifyQueryBuilder($queryBuilder, new ProductAvailability($locationId, $priceCategoryId));

        $queryBuilder->where($queryBuilder->expr()->eq('p.id', $productId));

        $query = $queryBuilder->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param $categoryId
     * @param $locationId
     * @param $priceCategoryId
     * @param array|null $priceRange
     * @return array
     */
    public function getAvailableProductIds($categoryId, $locationId, $priceCategoryId, array $priceRange = null)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('DISTINCT p.id')
            ->from(Product::class, 'p');

        $this->modifyQueryBuilder($queryBuilder, new ProductAvailability($locationId, $priceCategoryId, $priceRange));

        $queryBuilder
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.siteCategories', 'sc', Join::WITH, 'sc.id = ?1')
            ->setParameter(1, $categoryId);

        $query = $queryBuilder->getQuery();

        return $query->getResult('FetchColumnHydrator');
    }

    /**
     * @param $categoryId
     * @param $locationId
     * @param $priceCategoryId
     * @param null $paged
     * @param int $itemCountPerPage
     * @return array|Paginator
     */
    public function getAvailableProductsByCategoryId($categoryId, $locationId, $priceCategoryId, $paged = null, $itemCountPerPage = 50)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('p, pr, pa, pd, pp')
            ->from(Product::class, 'p');

        $this->modifyQueryBuilder($queryBuilder, new ProductAvailability($locationId, $priceCategoryId));

        $queryBuilder
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.siteCategories', 'sc', Join::WITH, 'sc.id = ?1')
            ->setParameter(1, $categoryId);

        $query = $queryBuilder->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator
                ->setCurrentPageNumber((int) $paged)
                ->setItemCountPerPage($itemCountPerPage)
                ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    /**
     * @param array $productIds
     * @param $locationId
     * @param $priceCategoryId
     * @param array $sorting
     * @param null $paged
     * @param int $itemCountPerPage
     * @return array|Paginator
     */
    public function getAvailableProductsByIds(array $productIds, $locationId, $priceCategoryId, $sorting = [],$paged = null, $itemCountPerPage = 50)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('p, pr, pa, pd, pp')
            ->from(Product::class, 'p');

        $this->modifyQueryBuilder($queryBuilder, new ProductAvailability($locationId, $priceCategoryId));

        $queryBuilder->where($queryBuilder->expr()->in('p.id', $productIds));

        if (isset($sorting['column']) && isset($sorting['direction'])) {
            switch ($sorting['column']) {
                case 'price':
                    $queryBuilder->orderBy('pp.value', $sorting['direction']);
                    break;
            }
        }

        $query = $queryBuilder->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator
                ->setCurrentPageNumber((int) $paged)
                ->setItemCountPerPage($itemCountPerPage)
                ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    /**
     * @param $productIds
     * @return array
     */
    public function getProductsByIds(array $productIds)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $query = $queryBuilder
            ->select('p')
            ->from(Product::class, 'p', 'p.id')
            ->where($queryBuilder->expr()->in('p.id', $productIds))
            ->getQuery()
            ->setFetchMode(Product::class, 'brand', \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);

        return $query->getResult();
    }
}