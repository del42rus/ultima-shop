<?php

namespace Ultima\Catalog\Repository\Specification;

use Rb\Specification\Doctrine\Condition\Between;
use Rb\Specification\Doctrine\Condition\Equals;
use Rb\Specification\Doctrine\Condition\EqualsProperty;
use Rb\Specification\Doctrine\Condition\GreaterThan;
use Rb\Specification\Doctrine\Logic\AndX;
use Rb\Specification\Doctrine\Query\LeftJoin;
use Doctrine\ORM\Query\Expr;
use Rb\Specification\Doctrine\Query\InnerJoin;
use Rb\Specification\Doctrine\Specification;

class ProductAvailability extends Specification
{
    public function __construct($locationId, $priceCategoryId = null, $priceRange = null)
    {
        $specs = [
            new InnerJoin('p.remains', 'pr'),
            new InnerJoin('pr.store', 's'),
            (new InnerJoin('s.location', 'l'))
                ->setConditionType(Expr\Join::WITH)
                ->setCondition(new Equals('l.id', $locationId)),
            (new InnerJoin('p.dates', 'pd'))
                ->setConditionType(Expr\Join::WITH)
                ->setCondition(new Equals('pd.location', $locationId)),
            new LeftJoin('p.arrivals', 'pa'),
            (new InnerJoin('pa.office', 'o'))
                ->setConditionType(Expr\Join::WITH)
                ->setCondition(new Equals('o.location', $locationId))
        ];

        if ($priceCategoryId) {
            $specs[] = (new InnerJoin('p.prices', 'pp'))
                ->setConditionType(Expr\Join::WITH)
                ->setCondition(new AndX(
                    new EqualsProperty('pp.zoneId', 'l.zoneId'),
                    new Equals('pp.priceCategoryId', $priceCategoryId),
                    $priceRange ? new Between('pp.value', $priceRange[0], $priceRange[1]) : new GreaterThan('pp.value', 0)
                ));
        }

        parent::__construct($specs);
    }
}