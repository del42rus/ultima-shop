<?php

namespace Ultima\Catalog\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Rb\Specification\Doctrine\SpecificationRepositoryTrait;
use Ultima\Catalog\Entity\Price;
use Ultima\Catalog\Repository\Specification\ProductAvailability;

class PriceRepository extends EntityRepository implements PriceRepositoryInterface
{
    use SpecificationRepositoryTrait;

    /**
     * @param array $productIds
     * @param $zoneId
     * @param $priceCategoryId
     * @return mixed
     */
    public function getPriceRangeByProductIds(array $productIds, $zoneId, $priceCategoryId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('MIN(p.value) as min_price, MAX(p.value) as max_price')
            ->from(Price::class, 'p')
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->in('p.product', $productIds),
                $queryBuilder->expr()->eq('p.zoneId', $zoneId),
                $queryBuilder->expr()->eq('p.priceCategoryId', $priceCategoryId)
            ));

        $query = $queryBuilder
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true);

        return $query->getSingleResult();
    }

    /**
     * @param $categoryId
     * @param $locationId
     * @param $zoneId
     * @param $priceCategoryId
     * @return mixed
     */
    public function getPriceRangeByCategoryId($categoryId, $locationId, $zoneId, $priceCategoryId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('MIN(pp.value) as min_price, MAX(pp.value) as max_price')
            ->from(Price::class, 'pp')
            ->innerJoin('pp.product', 'p');

        $this->modifyQueryBuilder($queryBuilder, new ProductAvailability($locationId));

        $queryBuilder
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.siteCategories', 'sc', Join::WITH, 'sc.id = ?1')
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->gt('pp.value', 0),
                $queryBuilder->expr()->eq('pp.zoneId', $zoneId),
                $queryBuilder->expr()->eq('pp.priceCategoryId', $priceCategoryId)
            ))
            ->setParameter(1, $categoryId);

        $query = $queryBuilder
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true);

        return $query->getSingleResult();
    }
}