<?php

namespace Ultima\Catalog\Repository;

use Doctrine\ORM\EntityRepository;
use Ultima\Catalog\Entity\Brand;

class BrandRepository extends EntityRepository implements BrandRepositoryInterface
{
    /**
     * @return array
     */
    public function getBrands()
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $query = $queryBuilder
                    ->select('b.name')
                    ->from(Brand::class, 'b')
                    ->getQuery();

        return $query->getArrayResult();
    }

    /**
     * @return bool|string
     */
    public function getBrandsCount()
    {
        $connection = $this->_em->getConnection();
        $queryBuilder = $connection->createQueryBuilder();

        return $queryBuilder
            ->select('COUNT(*)')
            ->from('#brands#')
            ->execute()
            ->fetchColumn();
    }
}