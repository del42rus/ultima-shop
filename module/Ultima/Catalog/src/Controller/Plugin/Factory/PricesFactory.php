<?php

namespace Ultima\Catalog\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Ultima\Catalog\Controller\Plugin\Prices;
use Ultima\Catalog\Service\PricesService;
use Zend\ServiceManager\Factory\FactoryInterface;

class PricesFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $pricesService = $container->get(PricesService::class);

        return new Prices($pricesService);
    }
}