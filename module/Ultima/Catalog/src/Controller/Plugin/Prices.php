<?php

namespace Ultima\Catalog\Controller\Plugin;

use Ultima\Catalog\Service\PricesService;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Prices extends AbstractPlugin
{
    /**
     * @var PricesService
     */
    private $pricesService;

    public function __construct(PricesService $pricesService)
    {
        $this->pricesService = $pricesService;
    }

    /**
     * @return PricesService
     */
    public function __invoke()
    {
        return $this->pricesService;
    }
}