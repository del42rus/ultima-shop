<?php

namespace  Ultima\Catalog\Navigation\Service;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Entity\SiteCategory;
use Ultima\Catalog\Repository\SiteCategoryRepositoryInterface;
use Ultima\Catalog\Service\PricesService;
use Ultima\Offices\Service\LocationService;
use Zend\Navigation\Page;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Navigation\Navigation;

class CatalogNavigationFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /** @var SiteCategoryRepositoryInterface $siteCategoryRepository */
        $siteCategoryRepository = $entityManager->getRepository(SiteCategory::class);

        /** @var LocationService $locationService */
        $locationService = $container->get(LocationService::class);
        $locationId = $locationService->getCurrentLocationId();

        /** @var PricesService $pricesService */
        $pricesService = $container->get(PricesService::class);
        $priceCategoryId = $pricesService->getPriceCategoryId();

        Page\Mvc::setDefaultRouter($container->get('router'));

        $pages = $this->getPages($siteCategoryRepository, $locationId, $priceCategoryId);

        return new Navigation($pages);
    }

    private function getPages(SiteCategoryRepositoryInterface $siteCategoryRepository, $locationId, $priceCategoryId)
    {
        $categories = $siteCategoryRepository->getAllCategories();
        $availableCategoryIds = $siteCategoryRepository->getAvailableCategoryIds($locationId, $priceCategoryId);

        $result = [
            'pages' => []
        ];

        /** @var SiteCategory $category */
        foreach ($categories as $category) {
            if (!in_array($category->getId(), $availableCategoryIds)) {
                continue;
            }

            $parents = array_filter(
                $categories, function (SiteCategory $value) use($category) {
                return ($value->getLeftKey() < $category->getLeftKey()) && ($value->getRightKey() > $category->getRightKey()) && ($value->getLevel() < $category->getLevel());
            });

            usort($parents, function ($a, $b) {
                return $a->getLevel() <=> $b->getLevel();
            });

            $pages = &$result;

            /** @var SiteCategory $parent */
            foreach ($parents as $parent) {
                if (!isset($pages['pages'][$parent->getId()])) {
                    $pages['pages'][$parent->getId()] = $this->createPage($parent);
                }

                $pages = &$pages['pages'][$parent->getId()];
            }

            $pages['pages'][$category->getId()] = $this->createPage($category);
        }

        return $result['pages'];
    }

    private function createPage(SiteCategory $category)
    {
        return [
            'label' => $category->getName(),
            'title' => $category->getName(),
            'route' => 'category',
            'params' => ['id' => $category->getId()],
            'order' => $category->getSortIndex()
        ];
    }
}