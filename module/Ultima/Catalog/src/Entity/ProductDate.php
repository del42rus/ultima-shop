<?php

namespace Ultima\Catalog\Entity;

use Ultima\Offices\Entity\Location;

class ProductDate
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Location
     */
    private $location;

    /**
     * @var \DateTime
     */
    private $pickupDate;

    /**
     * @var \DateTime
     */
    private $deliveryDate;

    /**
     * @var \DateTime
     */
    private $suborderDeliveryDate;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Location
     */
    public function getLocation(): Location
    {
        return $this->location;
    }

    /**
     * @param null $format
     * @return \DateTime|string
     */
    public function getPickupDate($format = null)
    {
        if (null !== $format) {
            return $this->pickupDate->format($format);
        }

        return $this->pickupDate;
    }

    /**
     * @param null $format
     * @return \DateTime|string
     */
    public function getDeliveryDate($format = null)
    {
        if (null !== $format) {
            return $this->deliveryDate->format($format);
        }

        return $this->deliveryDate;
    }

    /**
     * @param null $format
     * @return \DateTime|string
     */
    public function getSuborderDeliveryDate($format = null)
    {
        if (null !== $format) {
            return $this->suborderDeliveryDate->format($format);
        }

        return $this->suborderDeliveryDate;
    }
}