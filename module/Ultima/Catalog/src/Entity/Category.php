<?php

namespace Ultima\Catalog\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Category
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Category
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var ArrayCollection
     */
    private $siteCategories;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->siteCategories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Category
     */
    public function getParent(): Category
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    /**
     * @param bool $recursive
     * @return array
     */
    public function getChildrenIds($recursive = true)
    {
        $ids = [];

        foreach ($this->children as $children) {
            $ids[] = $children->getId();

            if (true === $recursive) {
                $ids = array_merge($ids, $children->getChildrenIds(true));
            }
        }

        return $ids;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    /**
     * @return array
     */
    public function getParentCategories()
    {
        $categories = [$this];

        if (null === $this->getParent()) {
            return $categories;
        } else {
            $parent = $this->getParent();
            $categories = array_merge($categories, $parent->getParentCategories());
        }

        return array_reverse($categories);
    }

    /**
     * @return ArrayCollection
     */
    public function getSiteCategories()
    {
        return $this->siteCategories;
    }
}