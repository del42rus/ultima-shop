<?php

namespace Ultima\Catalog\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Ultima\Offices\Entity\Location;

class Product
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var float
     */
    private $volume;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $warrantyPeriod;

    /**
     * @var WarrantyPeriodUnit
     */
    private $warrantyPeriodUnit;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var Product
     */
    private $originalProduct;

    /**
     * @var ArrayCollection
     */
    private $remains;

    /**
     * @var ArrayCollection
     */
    private $arrivals;

    /**
     * @var ArrayCollection
     */
    private $dates;

    /**
     * @var ArrayCollection
     */
    private $prices;

    /**
     * @var float
     */
    private $price;

    public function __construct()
    {
        $this->remains = new ArrayCollection();
        $this->arrivals = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->dates = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getWarrantyPeriod(): int
    {
        return $this->warrantyPeriod;
    }

    /**
     * @return WarrantyPeriodUnit
     */
    public function getWarrantyPeriodUnit(): WarrantyPeriodUnit
    {
        return $this->warrantyPeriodUnit;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return Product
     */
    public function getOriginalProduct(): Product
    {
        return $this->originalProduct;
    }

    /**
     * @return ArrayCollection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param null $priceCategoryId
     * @param null $zoneId
     * @return mixed
     */
    public function getPrice($priceCategoryId = null, $zoneId = null)
    {
        if (!isset($this->price)) {
            if (null != $priceCategoryId && null !== $zoneId) {
                $criteria = Criteria::create()
                    ->where(Criteria::expr()->eq('priceCategoryId', $priceCategoryId))
                    ->andWhere(Criteria::expr()->eq('zoneId', $zoneId));

                return $this->prices->matching($criteria)->first();
            }

            $this->price = $this->prices->first();
        }

        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection
     */
    public function getRemains()
    {
        return $this->remains;
    }

    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
        $quantity = 0;

        /** @var ProductRemain $remain */
        foreach ($this->remains as $remain) {
            $quantity += $remain->getRemainsQuantity() - $remain->getReserveQuantity();
        }

        return $quantity;
    }

    /**
     * @return ArrayCollection
     */
    public function getArrivals()
    {
        return $this->arrivals;
    }

    /**
     * @return int
     */
    public function getSuborderQuantity()
    {
        $quantity = 0;

        /** @var ProductArrival $arrival */
        foreach ($this->arrivals as $arrival) {
            $quantity += $arrival->getQuantity();
        }

        return $quantity;
    }

    /**
     * @return ArrayCollection
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param null $locationId
     * @return mixed
     */
    public function getDate($locationId = null) {
        if (null !== $locationId) {
            return $this->dates->filter(function (ProductDate $date) use ($locationId) {
                return $date->getLocation()->getId() == $locationId;
            })->first();
        }

        return $this->dates->first();
    }

    /**
     * @param null $format
     * @param null $locationId
     * @return mixed
     */
    public function getPickupDate($format = null, $locationId = null)
    {
        return $this->getDate($locationId)->getPickupDate($format);
    }

    /**
     * @param null $format
     * @param null $locationId
     * @return mixed
     */
    public function getDeliveryDate($format = null, $locationId = null)
    {
        return $this->getDate($locationId)->getDeliveryDate($format);
    }

    /**
     * @param null $format
     * @param null $locationId
     * @return mixed
     */
    public function getSuborderDeliveryDate($format = null, $locationId = null)
    {
        return $this->getDate($locationId)->getSuborderDeliveryDate($format);
    }
}