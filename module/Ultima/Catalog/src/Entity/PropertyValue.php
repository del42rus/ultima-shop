<?php

namespace Ultima\Catalog\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class PropertyValue
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var integer
     */
    private $sortIndex;

    /**
     * @var PropertyValue
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var Property
     */
    private $property;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getSortIndex(): int
    {
        return $this->sortIndex;
    }

    /**
     * @return PropertyValue
     */
    public function getParent(): PropertyValue
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return Property
     */
    public function getProperty(): Property
    {
        return $this->property;
    }
}