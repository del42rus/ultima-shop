<?php

namespace Ultima\Catalog\Entity;

use Ultima\Offices\Entity\Store;

class ProductRemain
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Store
     */
    private $store;

    /**
     * @var integer
     */
    private $remainsQuantity;

    /**
     * @var integer
     */
    private $reserveQuantity;

    /**
     * @var integer
     */
    private $sellSpeed;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return int
     */
    public function getRemainsQuantity(): int
    {
        return $this->remainsQuantity;
    }

    /**
     * @return int
     */
    public function getReserveQuantity(): int
    {
        return $this->reserveQuantity;
    }

    /**
     * @return int
     */
    public function getSellSpeed(): int
    {
        return $this->sellSpeed;
    }
}