<?php

namespace Ultima\Catalog\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Property
{
    /**#@+
     * @const integer TYPE constant names
     */
    const PROPERTY_TYPE_NUMERIC = 1;
    const PROPERTY_TYPE_BOOLEAN = 2;
    const PROPERTY_TYPE_STRING = 3;
    const PROPERTY_TYPE_TEXT = 4;
    const PROPERTY_TYPE_ONE_OF_MANY = 5;
    const PROPERTY_TYPE_MANY_OF_MANY = 6;
    const PROPERTY_TYPE_COMPOSITE = 7;
    /**#@-*/

    /**#@+
     * @const integer KIND constant names
     */
    const PROPERTY_KIND_REGULAR = 1;
    const PROPERTY_KIND_FILTER = 2;
    const PROPERTY_KIND_NAVIGATION = 3;
    const PROPERTY_KIND_FILTER4 = 4;
    /**#@-*/

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Property
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var integer
     */
    private $typeId;

    /**
     * @var
     */
    private $kindId;

    /**
     * @var boolean
     */
    private $hidden;

    /**
     * @var boolean
     */
    private $sortValues;

    /**
     * @var PropertyUnit
     */
    private $unit;

    /**
     * @var ArrayCollection
     */
    private $values;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->values = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Property
     */
    public function getParent(): Property
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->typeId;
    }

    /**
     * @return mixed
     */
    public function getKindId()
    {
        return $this->kindId;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return bool
     */
    public function isSortValues(): bool
    {
        return $this->sortValues;
    }

    /**
     * @return null|PropertyUnit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function isBoolean()
    {
        return $this->typeId == self::PROPERTY_TYPE_BOOLEAN;
    }

    public function isNumeric()
    {
        return $this->typeId == self::PROPERTY_TYPE_NUMERIC;
    }
}