<?php

namespace Ultima\Catalog\Entity;

class Price
{
    const DEFAULT_PRICE_CATEGORY_ID = 1;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var integer
     */
    private $priceCategoryId;

    /**
     * @var integer
     */
    private $zoneId;

    /**
     * @var float
     */
    private $value;

    /**
     * @var float
     */
    private $prevValue;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getPriceCategoryId(): int
    {
        return $this->priceCategoryId;
    }

    /**
     * @return int
     */
    public function getZoneId(): int
    {
        return $this->zoneId;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return float
     */
    public function getPrevValue(): float
    {
        return $this->prevValue;
    }
}