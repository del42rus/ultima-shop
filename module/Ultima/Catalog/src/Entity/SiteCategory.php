<?php

namespace Ultima\Catalog\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class SiteCategory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $urlName;

    /**
     * @var string
     */
    private $buyName;

    /**
     * @var integer
     */
    private $sortIndex;

    /**
     * @var SiteCategory
     */
    private $parent;

    /**
     * @var SiteCategory
     */
    private $original;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var integer
     */
    private $level;

    /**
     * @var integer
     */
    private $leftKey;

    /**
     * @var integer
     */
    private $rightKey;

    /**
     * @var ArrayCollection
     */
    private $nativeCategories;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->nativeCategories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return SiteCategory
     */
    public function getParent(): SiteCategory
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    /**
     * @param bool $recursive
     * @return array
     */
    public function getChildrenIds($recursive = true)
    {
        $ids = [];

        foreach ($this->children as $children) {
            $ids[] = $children->getId();

            if (true === $recursive) {
                $ids = array_merge($ids, $children->getChildrenIds(true));
            }
        }

        return $ids;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    /**
     * @return array
     */
    public function getParentCategories()
    {
        $categories = [$this];

        if (null === $this->getParent()) {
            return $categories;
        } else {
            $parent = $this->getParent();
            $categories = array_merge($categories, $parent->getParentCategories());
        }

        return array_reverse($categories);
    }

    /**
     * @return string
     */
    public function getUrlName(): string
    {
        return $this->urlName;
    }

    /**
     * @return string
     */
    public function getBuyName(): string
    {
        return $this->buyName;
    }

    /**
     * @return int
     */
    public function getSortIndex(): int
    {
        return $this->sortIndex;
    }

    /**
     * @return SiteCategory
     */
    public function getOriginal(): SiteCategory
    {
        return $this->original;
    }

    /**
     * @return ArrayCollection
     */
    public function getNativeCategories()
    {
        return $this->nativeCategories;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getLeftKey(): int
    {
        return $this->leftKey;
    }

    /**
     * @return int
     */
    public function getRightKey(): int
    {
        return $this->rightKey;
    }
}