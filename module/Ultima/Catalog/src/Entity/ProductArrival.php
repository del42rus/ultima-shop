<?php

namespace Ultima\Catalog\Entity;

use Ultima\Offices\Entity\Office;

class ProductArrival
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Office
     */
    private $office;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $arrivalDate;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return \DateTime
     */
    public function getArrivalDate(): \DateTime
    {
        return $this->arrivalDate;
    }
}