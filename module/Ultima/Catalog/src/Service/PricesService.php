<?php

namespace Ultima\Catalog\Service;

use Ultima\Catalog\Entity\Price;

class PricesService
{
    public function getPriceCategoryId()
    {
        return Price::DEFAULT_PRICE_CATEGORY_ID;
    }
}