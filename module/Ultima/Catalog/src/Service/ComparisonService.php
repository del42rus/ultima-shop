<?php

namespace Ultima\Catalog\Service;

use Ultima\Catalog\Comparison\Storage\Session as Storage;
use Ultima\Catalog\Entity\Property;
use Ultima\Catalog\Repository\ProductRepositoryInterface;
use Ultima\Catalog\Repository\PropertyRepositoryInterface;

class ComparisonService
{
    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var PropertyRepositoryInterface
     */
    private $propertyRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(Storage $storage, PropertyRepositoryInterface $propertyRepository, ProductRepositoryInterface $productRepository)
    {
        $this->storage = $storage;
        $this->propertyRepository = $propertyRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @return Storage
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param $productId
     * @param $categoryId
     * @param $categoryName
     */
    public function add($productId, $categoryId, $categoryName)
    {
        $this->storage->add($productId, $categoryId, $categoryName);
    }

    /**
     * @param $productId
     * @param $categoryId
     * @return bool
     */
    public function remove($productId, $categoryId)
    {
        return $this->storage->remove($productId, $categoryId);
    }

    /**
     * @return array|mixed
     */
    public function getCategories()
    {
        return $this->storage->getCategories();
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getProducts($categoryId)
    {
        $productIds = $this->storage->getProductIds($categoryId);

        if (empty($productIds)) {
            return [];
        }

        return $this->productRepository->getProductsByIds($productIds);
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getProductsProperties($categoryId)
    {
        $result['products'] = [];
        $result['properties'] = [];

        $productIds = $this->storage->getProductIds($categoryId);

        if (empty($productIds)) {
            return $result;
        }

        $productsPropertiesValues = $this->propertyRepository->getProductsPropertiesValues($productIds);

        if (empty($productsPropertiesValues)) {
            return $result;
        }

        $valueIds = array_unique(array_map(function($value) { return $value['property_value_id']; }, $productsPropertiesValues));
        $propertyIds = array_unique(array_map(function($value) { return $value['property_id']; }, $productsPropertiesValues));

        $properties = $this->propertyRepository->getPropertiesByIds($propertyIds, $valueIds);

        $props = [];

        foreach ($properties as $property) {
            $props[$property->getId()]['name'] = $property->getName();
            $props[$property->getId()]['type'] = $property->getTypeId();

            foreach ($property->getValues() as $value) {
                $props[$property->getId()]['values'][$value->getId()] = $value->getValue();
            }
        }

        foreach ($productsPropertiesValues as $productPropertyValue) {
            $productId = $productPropertyValue['product_id'];
            $propertyId = $productPropertyValue['property_id'];
            $property = $props[$propertyId];

            switch ($property['type']) {
                case Property::PROPERTY_TYPE_NUMERIC:
                    $propertyValue = $productPropertyValue['value_number'];
                    $result['products'][$productId][$propertyId] = $propertyValue;
                    break;
                case Property::PROPERTY_TYPE_BOOLEAN;
                    $propertyValue = $productPropertyValue['value_boolean'];
                    $result['products'][$productId][$propertyId] = $propertyValue;
                    break;
                case Property::PROPERTY_TYPE_STRING:
                    $propertyValue = $productPropertyValue['value_string'];
                    $result['products'][$productId][$propertyId] = $propertyValue;
                    break;
                case Property::PROPERTY_TYPE_TEXT:
                    $propertyValue = $productPropertyValue['value_text'];
                    $result['products'][$productId][$propertyId] = $propertyValue;
                    break;
                default:
                    $propertyValues = $property['values'];
                    $propertyValueId = $productPropertyValue['property_value_id'];
                    $propertyValue = $propertyValues[$propertyValueId];

                    $result['products'][$productId][$propertyId][] = $propertyValue;
            }

            if (!isset($result['properties'][$propertyId])) {
                $result['properties'][$propertyId]['values'] = [];
                $result['properties'][$propertyId]['name'] = $property['name'];
            }
        }

        foreach ($result['products'] as $propertiesValues) {
            foreach (array_keys($result['properties']) as $propertyId) {
                $propertyValue = $propertiesValues[$propertyId] ?? null;

                if (is_array($propertyValue)) {
                    sort($propertyValue);
                    $propertyValue = join(',', $propertyValue);
                }

                if (!in_array($propertyValue, $result['properties'][$propertyId]['values'])) {
                    $result['properties'][$propertyId]['values'][] = $propertyValue;
                }
            }
        }

        return $result;
    }

    /**
     * @param $productId
     * @return bool
     */
    public function hasProduct($productId)
    {
        return $this->storage->hasProduct($productId);
    }
}