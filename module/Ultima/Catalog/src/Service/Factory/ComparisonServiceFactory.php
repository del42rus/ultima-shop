<?php

namespace Ultima\Catalog\Service\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Comparison\Storage\Session as Storage;
use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Entity\Property;
use Ultima\Catalog\Repository\ProductRepositoryInterface;
use Ultima\Catalog\Repository\PropertyRepositoryInterface;
use Ultima\Catalog\Service\ComparisonService;
use Zend\ServiceManager\Factory\FactoryInterface;

class ComparisonServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $storage = new Storage();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        /** @var PropertyRepositoryInterface $propertyRepository */
        $propertyRepository = $entityManager->getRepository(Property::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $entityManager->getRepository(Product::class);

        return new ComparisonService($storage, $propertyRepository, $productRepository);
    }
}