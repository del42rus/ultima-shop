<?php

namespace Ultima\Catalog\Service\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Entity\SiteCategory;
use Ultima\Catalog\Repository\ProductRepositoryInterface;
use Ultima\Catalog\Repository\SiteCategoryRepositoryInterface;
use Ultima\Catalog\Service\CatalogService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CatalogServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /** @var SiteCategoryRepositoryInterface $siteCategoryRepository */
        $siteCategoryRepository = $entityManager->getRepository(SiteCategory::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $entityManager->getRepository(Product::class);

        return new CatalogService($siteCategoryRepository, $productRepository);
    }
}