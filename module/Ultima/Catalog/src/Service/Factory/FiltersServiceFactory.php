<?php

namespace Ultima\Catalog\Service\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Entity\Price;
use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Entity\Property;
use Ultima\Catalog\Repository\PriceRepositoryInterface;
use Ultima\Catalog\Repository\ProductRepositoryInterface;
use Ultima\Catalog\Repository\PropertyRepositoryInterface;
use Ultima\Catalog\Service\FiltersService;
use Zend\ServiceManager\Factory\FactoryInterface;

class FiltersServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /** @var PropertyRepositoryInterface $propertyRepository */
        $propertyRepository = $entityManager->getRepository(Property::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $entityManager->getRepository(Product::class);
        /** @var PriceRepositoryInterface $priceRepository */
        $priceRepository = $entityManager->getRepository(Price::class);

        $filterService = new FiltersService($propertyRepository, $productRepository, $priceRepository);

        return $filterService;
    }
}