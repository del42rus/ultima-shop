<?php

namespace Ultima\Catalog\Service;

use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Entity\Property;
use Ultima\Catalog\Entity\PropertyUnit;
use Ultima\Catalog\Entity\PropertyValue;
use Ultima\Catalog\Filter\Filter;
use Ultima\Catalog\Filter\FilterSet;
use Ultima\Catalog\Filter\FilterValue;
use Ultima\Catalog\Filter\RangeFilter;
use Ultima\Catalog\Repository\PriceRepositoryInterface;
use Ultima\Catalog\Repository\ProductRepositoryInterface;
use Ultima\Catalog\Repository\PropertyRepositoryInterface;

class FiltersService
{
    /**
     * @var PropertyRepositoryInterface
     */
    private $propertyRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var PriceRepositoryInterface
     */
    private $priceRepository;

    public function __construct(PropertyRepositoryInterface $propertyRepository, ProductRepositoryInterface $productRepository, PriceRepositoryInterface $priceRepository)
    {
        $this->propertyRepository = $propertyRepository;
        $this->productRepository = $productRepository;
        $this->priceRepository = $priceRepository;
    }

    public function setupFeatureFilters(FilterSet $filterSet, array $productIds)
    {
        $productsFiltersValues = $this->propertyRepository->getProductsFiltersValues($productIds);

        if (empty($productsFiltersValues)) {
            return;
        }

        $valueIds = array_unique(array_map(function($value) { return $value['property_value_id']; }, $productsFiltersValues));
        $filterIds = array_unique(array_map(function($value) { return $value['property_id']; }, $productsFiltersValues));

        $properties = $this->propertyRepository->getPropertiesByIds($filterIds, $valueIds);

        /** @var Property $property */
        foreach ($properties as $property) {
            $filter = $property->isNumeric() ? new RangeFilter() : new Filter();

            $filter->setId($property->getId());
            $filter->setName($property->getName());
            $filter->setTypeId($property->getTypeId());

            if ($property->getUnit() instanceof PropertyUnit) {
                $filter->setUnit($property->getUnit()->getName());
            }

            if (!$property->isBoolean() && !$property->isNumeric()) {
                /** @var PropertyValue $value */
                foreach ($property->getValues() as $value) {
                    $filterValue = new FilterValue();
                    $filterValue->setId($value->getId());
                    $filterValue->setValue($value->getValue());

                    $filter->addValue($filterValue);
                }
            }

            $filterSet->addFilter($filter);

            if ($property->isSortValues()) {
                $filter->sortValues();
            }
        }

        foreach ($productsFiltersValues as $productFilterValue) {
            $filter = $filterSet->getFilter($productFilterValue['property_id']);

            if (!$filter instanceof Filter) {
                continue;
            }

            $filterProduct = $filterSet->addProduct($productFilterValue['product_id']);

            switch ($filter->getTypeId()) {
                case Property::PROPERTY_TYPE_NUMERIC:
                    $filterValue = $filter->getValue($productFilterValue['value_number']);

                    if (!$filterValue instanceof FilterValue) {
                        $filterValue = new FilterValue();
                        $filterValue->setValue($productFilterValue['value_number']);
                        $filterValue->setId($filter->getId() . '-' . $filterValue->getValue());

                        $filter->addValue($filterValue);
                    }

                    $filterValue->addProduct($filterProduct);
                    break;
                case Property::PROPERTY_TYPE_BOOLEAN:
                    if ($productFilterValue['value_boolean']) {
                        $filterValueYes = $filter->getValue(1);

                        if (!$filterValueYes instanceof FilterValue) {
                            $filterValueYes = new FilterValue();
                            $filterValueYes->setValue(1);
                            $filterValueYes->setId($filter->getId() . '-' . $filterValueYes->getValue());

                            $filter->addValue($filterValueYes);
                        }

                        $filterValueYes->addProduct($filterProduct);
                    } else {
                        $filterValueNo = $filter->getValue(0);

                        if ($filterValueNo) {
                            $filterValueNo = new FilterValue();
                            $filterValueNo->setValue(0);
                            $filterValueNo->setId($filter->getId() . '-' . $filterValueNo->getValue());

                            $filter->addValue($filterValueNo);
                        }

                        $filterValueNo->addProduct($filterProduct);
                    }
                    break;
                default:
                    $filterValue = $filter->getValue($productFilterValue['property_value_id']);

                    if ($filterValue instanceof FilterValue) {
                        $filterValue->addProduct($filterProduct);
                    }
            }
        }
    }

    public function setupBrandsFilter(FilterSet $filterSet, array $productIds)
    {
        $filter = new Filter();
        $filter->setId('brands');
        $filter->setName('Brands');

        $products = $this->productRepository->getProductsByIds($productIds);

        /** @var Product $product */
        foreach ($products as $product) {
            $brand = $product->getBrand();

            $filterValue = $filter->getValue($brand->getId());

            if (!$filterValue instanceof FilterValue) {
                $filterValue = new FilterValue();
                $filterValue->setId($brand->getId());
                $filterValue->setValue($brand->getName());

                $filter->addValue($filterValue);
            }

            $filterValue->addProduct($filterSet->addProduct($product->getId()));
        }

        $filterSet->addFilter($filter);
        $filter->sortValues();
    }

    public function setupPriceFilter(FilterSet $filterSet, $categoryId, $productIds, $locationId, $zoneId, $priceCategoryId)
    {
        $filter = new RangeFilter();
        $filter->setId('price');
        $filter->setName('Price');

        $priceRange = ['min_price' => 0, 'max_price' => 0];

        if ($categoryId) {
            $priceRange = $this->priceRepository->getPriceRangeByCategoryId($categoryId, $locationId, $zoneId, $priceCategoryId);
        } elseif (!empty($productIds)) {
            $priceRange = $this->priceRepository->getPriceRangeByProductIds($productIds, $zoneId, $priceCategoryId);
        }

        $filter->setMinValue((int) $priceRange['min_price']);
        $filter->setMaxValue((int) $priceRange['max_price']);

        $filterSet->addFilter($filter);
    }

    public function extractSelectedFilters(FilterSet $filterSet, array $params = [])
    {
        foreach ($filterSet as $filter) {
             if ($filter instanceof RangeFilter) {
                $minValue = $params['min'][$filter->getId()] ?? 0;
                $maxValue = $params['max'][$filter->getId()] ?? 0;

                if ($minValue && $maxValue && ($minValue <= $maxValue)) {
                    $filter->selectValue([$minValue, $maxValue]);
                }
            } else {
                if (array_key_exists($filter->getId(), $params)) {
                    $values = $params[$filter->getId()];
                    foreach ($values as $value) {
                        $filter->selectValue($value);
                    }
                }
            }
        }
    }
}