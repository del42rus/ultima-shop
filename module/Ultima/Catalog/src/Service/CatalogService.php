<?php

namespace Ultima\Catalog\Service;

use Ultima\Catalog\Repository\ProductRepositoryInterface;
use Ultima\Catalog\Repository\PropertyRepositoryInterface;
use Ultima\Catalog\Repository\SiteCategoryRepositoryInterface;

class CatalogService
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SiteCategoryRepositoryInterface
     */
    private $siteCategoryRepository;

    public function __construct(
        SiteCategoryRepositoryInterface $siteCategoryRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->siteCategoryRepository = $siteCategoryRepository;
        $this->productRepository = $productRepository;
    }

    public function getProductById($productId)
    {
        return $this->productRepository->getProductById($productId);
    }

    public function getAvailableProductById($productId, $locationId, $priceCategoryId)
    {
        return $this->productRepository->getAvailableProductById($productId, $locationId, $priceCategoryId);
    }

    public function getAvailableProductIds($categoryId, $locationId, $priceCategoryId, $priceRange)
    {
        return $this->productRepository->getAvailableProductIds($categoryId, $locationId, $priceCategoryId, $priceRange);
    }

    public function getAvailableProductsByIds(array $productIds, $locationId, $priceCategoryId, $sorting = [],$paged = null, $itemCountPerPage = 50)
    {
        return $this->productRepository->getAvailableProductsByIds($productIds, $locationId, $priceCategoryId, $sorting, $paged, $itemCountPerPage);
    }

    public function getAvailableProductsByCategoryId($categoryId, $locationId, $priceCategoryId, $paged = null, $itemCountPerPage = 50)
    {
        return $this->productRepository->getAvailableProductsByCategoryId($categoryId, $locationId, $priceCategoryId, $paged, $itemCountPerPage);
    }

    public function getProductsByIds(array $productIds)
    {
        return $this->productRepository->getProductsByIds($productIds);
    }

    public function getCategoryById($categoryId)
    {
        return $this->siteCategoryRepository->getCategoryById($categoryId);
    }
}