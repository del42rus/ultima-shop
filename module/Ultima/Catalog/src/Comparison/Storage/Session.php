<?php

namespace Ultima\Catalog\Comparison\Storage;

use Zend\Session\Container;

/**
 * @property array|mixed categories
 */
class Session extends Container
{
    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->categories = $this->categories ?? [];
    }

    /**
     * @param $productId
     * @param $categoryId
     * @param $categoryName
     */
    public function add($productId, $categoryId, $categoryName)
    {
        $this->categories[$categoryId]['name'] = $categoryName;

        if (!isset($this->categories[$categoryId]['products'])) {
            $this->categories[$categoryId]['products'] = [];
        }

        if (!in_array($productId, $this->categories[$categoryId]['products'])) {
            $this->categories[$categoryId]['products'][] = $productId;
        }
    }

    /**
     * @param $productId
     * @param $categoryId
     * @return bool
     */
    public function remove($productId, $categoryId)
    {
        if (!isset($this->categories[$categoryId])) {
            return false;
        }

        $key = array_search($productId, $this->categories[$categoryId]['products']);

        if ($key === false) {
            return false;
        }

        unset($this->categories[$categoryId]['products'][$key]);

        if (empty($this->categories[$categoryId]['products'])) {
            unset($this->categories[$categoryId]);
        }

        return true;
    }

    /**
     * @return array|mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getProductIds($categoryId)
    {
        if (!isset($this->categories[$categoryId]['products'])) {
            return [];
        }

        return $this->categories[$categoryId]['products'];
    }

    /**
     * @param $productId
     * @return bool
     */
    public function hasProduct($productId)
    {
        foreach ($this->categories as $category) {
            foreach ($category['products'] as $product) {
                if ($product == $productId) {
                    return true;
                }
            }
        }

        return false;
    }
}
