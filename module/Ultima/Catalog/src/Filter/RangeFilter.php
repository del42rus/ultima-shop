<?php

namespace Ultima\Catalog\Filter;

class RangeFilter extends Filter
{
    /**
     * @var integer
     */
    private $min;

    /**
     * @var integer
     */
    private $max;

    /**
     * @var integer
     */
    private $step;

    /**
     * @var array
     */
    private $selectRange;

    /**
     * @param $minValue
     */
    public function setMinValue($minValue)
    {
        $this->min = $minValue;
    }

    /**
     * @param $maxValue
     */
    public function setMaxValue($maxValue)
    {
        $this->max = $maxValue;
    }

    /**
     * @return float|int
     */
    public function getMinValue()
    {
        if (!isset($this->min)) {
            $this->min = round(min(array_map(
                function(FilterValue $value) {
                    return $value->getValue();
                }, $this->values)
            ));
        }

        return $this->min;
    }

    /**
     * @return float|int
     */
    public function getMaxValue()
    {
        if (!isset($this->max)) {
            $this->max = round(max(array_map(
                function(FilterValue $value) {
                    return $value->getValue();
                }, $this->values)
            ));
        }

        return $this->max;
    }

    /**
     * @return float|int
     */
    public function getStepValue()
    {
        $step = ($this->getMaxValue() - $this->getMinValue()) / 28;

        if ($step < 0.1) {
            $this->step = 0.1;
        } elseif ($step < 1) {
            $this->step = round($step, 1);
        } elseif ($step > 1000) {
            $this->step = 1000;
        } else {
            $this->step = ceil($step);
        }

        return $this->step;
    }

    /**
     * @return mixed|null
     */
    public function getSelectRangeMax()
    {
        return $this->selectRange[1] ?? null;
    }

    /**
     * @return mixed|null
     */
    public function getSelectRangeMin()
    {
        return $this->selectRange[0] ?? null;
    }

    /**
     * @param $min
     * @param $max
     */
    public function setSelectRange($min, $max)
    {
        $this->selectRange = [$min, $max];
    }

    /**
     * @return array
     */
    public function getSelectRange()
    {
        return $this->selectRange;
    }

    /**
     * @param $value
     * @return bool
     */
    public function selectValue($value)
    {
        if (count($value) != 2) {
            return false;
        }

        list($min, $max) = $value;

        $this->setSelectRange($min, $max);

        /** @var FilterValue $value */
        foreach ($this->getValues() as $value) {
            if (((float) $value->getValue() >= (float) $min) && ((float) $value->getValue() <= (float) $max)) {
                $value->select();
            }
        }
    }
}