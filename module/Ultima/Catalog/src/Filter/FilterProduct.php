<?php

namespace Ultima\Catalog\Filter;

class FilterProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $filterValues = [];

    /**
     * @param null $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param FilterValue $filterValue
     */
    public function addFilterValue(FilterValue $filterValue)
    {
        $this->filterValues[$filterValue->getId()] = $filterValue;
    }

    /**
     * @return array
     */
    public function getFilterValues()
    {
        return $this->filterValues;
    }

    /**
     * @param array $filterValues
     * @return bool
     */
    public function hasAnyFilterValue(array $filterValues)
    {
        foreach ($filterValues as $filterValue) {
            if (array_key_exists($filterValue->getId(), $this->filterValues)) {
                return true;
            }
        }

        return false;
    }

}