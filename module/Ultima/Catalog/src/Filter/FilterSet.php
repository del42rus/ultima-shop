<?php

namespace Ultima\Catalog\Filter;

use ArrayIterator;
use IteratorAggregate;
use Countable;

class FilterSet implements IteratorAggregate, Countable
{
    /**
     * @var array
     */
    private $filters = [];

    /**
     * @var array
     */
    private $products = [];

    /**
     * @var array
     */
    private $selectedProductIds = null;

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->filters);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function addProduct($product)
    {
        $product = $product instanceof FilterProduct ? $product : new FilterProduct($product);

        if (!array_key_exists($product->getId(), $this->products)) {
            $this->products[$product->getId()] = $product;
        }

        return $this->products[$product->getId()];
    }

    /**
     * @param $productId
     * @return mixed|null
     */
    public function getProductById($productId)
    {
        return $this->products[$productId] ?? null;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Filter $filter
     */
    public function addFilter(Filter $filter)
    {
        $this->filters[$filter->getId()] = $filter;
        $filter->setFilterSet($this);
    }

    /**
     * @param $filterId
     * @return mixed|null
     */
    public function getFilter($filterId)
    {
        return $this->filters[$filterId] ?? null;
    }

    /**
     * @param $filterId
     * @param $value
     * @return bool
     */
    public function selectValue($filterId, $value)
    {
        $filter = $this->getFilter($filterId);

        if (!$filter instanceof Filter) {
            return false;
        }

        return $filter->selectValue($value);
    }

    /**
     * @return array
     */
    public function getProductIds()
    {
        return array_keys($this->products);
    }

    /**
     * @return array
     */
    public function getSelectedProductIds()
    {
        if (!isset($this->selectedProductIds)) {
            $this->selectedProductIds = [];

            /** @var FilterProduct $product */
            foreach ($this->products as $product) {
                $selected = true;

                /** @var Filter $filter */
                foreach ($this->filters as $filter) {
                    if (!$filter->countSelectedValues()) {
                        continue;
                    }
                    if (!$product->hasAnyFilterValue($filter->getSelectedValues())) {
                        $selected = false;
                    }
                }

                if ($selected) {
                    $this->selectedProductIds[] = $product->getId();
                }
            }
        }

        return $this->selectedProductIds;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->filters);
    }

    /**
     * @return bool
     */
    public function hasSelectedFilters()
    {
        /** @var Filter $filter */
        foreach ($this->filters as $filter) {
            if ($filter->hasSelectedValues()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public function countSelectedFilters()
    {
        $count = 0;
        /** @var Filter $filter */
        foreach ($this->filters as $filter) {
            if ($filter->hasSelectedValues()) {
                $count++;
            }
        }

        return $count;
    }
}