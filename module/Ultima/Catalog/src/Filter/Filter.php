<?php

namespace Ultima\Catalog\Filter;

use Ultima\Catalog\Entity\Property;

class Filter
{
    /**
     * @var int|string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $typeId;

    /**
     * @var string
     */
    private $unit;

    /**
     * @var array
     */
    protected $values = [];

    /**
     * @var FilterSet
     */
    private $filterSet;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     */
    public function setTypeId(int $typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return bool
     */
    public function isNumeric()
    {
        return $this->typeId == Property::PROPERTY_TYPE_NUMERIC;
    }

    /**
     * @return bool
     */
    public function isBoolean()
    {
        return $this->typeId == Property::PROPERTY_TYPE_BOOLEAN;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param FilterValue $value
     */
    public function addValue(FilterValue $value)
    {
        $this->values[$value->getId()] = $value;
        $value->setFilter($this);
    }

    /**
     * @param $value
     * @return FilterValue|null
     */
    public function getValue($value)
    {
        if ($this->isNumeric() || $this->isBoolean()) {
            $value = $this->getId() . '-' . $value;
        }

        return $this->values[$value] ?? null;
    }

    /**
     * @return array
     */
    public function getSelectedValues()
    {
        return array_filter($this->values, function(FilterValue $value) {
            return $value->isSelected();
        });
    }

    /**
     * @return int
     */
    public function countSelectedValues()
    {
        $count = 0;
        foreach ($this->values as $value) {
            if ($value->isSelected()) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return bool
     */
    public function hasSelectedValues()
    {
        foreach ($this->values as $value) {
            if ($value->isSelected()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param FilterSet $filterSet
     */
    public function setFilterSet(FilterSet $filterSet)
    {
        $this->filterSet = $filterSet;
    }

    /**
     * @return FilterSet
     */
    public function getFilterSet(): FilterSet
    {
        return $this->filterSet;
    }

    /**
     * @return array
     */
    public function getNotEmptyValues()
    {
        return array_filter($this->values, function(FilterValue $filterValue) {
            return $filterValue->getProductsCount() > 0;
        });
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->getNotEmptyValues()) == 0;
    }

    /**
     *
     */
    public function sortValues()
    {
        uasort($this->values, function(FilterValue $a, FilterValue $b) {
            return $b->getProductsCount() <=> $a->getProductsCount();
        });
    }

    /**
     * @param $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $value
     * @return bool
     */
    public function selectValue($value)
    {
        $filterValue = $this->getValue($value);

        if (!$filterValue instanceof FilterValue) {
            return false;
        }

        $filterValue->select();
    }
}