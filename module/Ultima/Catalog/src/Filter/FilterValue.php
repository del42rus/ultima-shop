<?php

namespace Ultima\Catalog\Filter;

class FilterValue
{
    /**
     * @var int|string
     */
    private $id;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var array
     */
    private $products = [];

    /**
     * @var bool
     */
    private $isSelected = false;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @param Filter $filter
     */
    public function setFilter(Filter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return Filter
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param FilterProduct $filterProduct
     */
    public function addProduct(FilterProduct $filterProduct)
    {
        $this->products[$filterProduct->getId()] = $filterProduct;
        $filterProduct->addFilterValue($this);
    }

    /**
     * @return int
     */
    public function getProductsCount()
    {
        $filterSet = $this->getFilter()->getFilterSet();

        if (!$filterSet->hasSelectedFilters() ||
            ($this->getFilter()->countSelectedValues() && $filterSet->countSelectedFilters() == 1)
        ) {
            return count($this->products);
        }

        $count = 0;

        /** @var FilterProduct $product */
        foreach ($this->products as $product) {
            if (in_array($product->getId(), $filterSet->getSelectedProductIds())) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return bool
     */
    public function isSelected()
    {
        return $this->isSelected;
    }

    public function select()
    {
        $this->isSelected = true;
    }
}