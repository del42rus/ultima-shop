<?php

namespace Ultima\Catalog;

use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Ultima\Catalog\Replicator\BrandsReplicator;
use Ultima\Catalog\Replicator\CategoriesReplicator;
use Ultima\Catalog\Replicator\Factory\PricesReplicatorFactory;
use Ultima\Catalog\Replicator\Factory\ProductRemainsReplicatorFactory;
use Ultima\Catalog\Replicator\Factory\ProductsReplicatorFactory;
use Ultima\Catalog\Replicator\Factory\PropertiesReplicatorFactory;
use Ultima\Catalog\Replicator\WarrantyPeriodUnitsReplicator;

return [
    'replicators' => [
        'brands' => BrandsReplicator::class,
        'products' => ProductsReplicatorFactory::class,
        'product_remains' => ProductRemainsReplicatorFactory::class,
        'categories' => CategoriesReplicator::class,
        'warranty_period_units' => WarrantyPeriodUnitsReplicator::class,
        'properties' => PropertiesReplicatorFactory::class,
        'prices' => PricesReplicatorFactory::class
    ],
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
];