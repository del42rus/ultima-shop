<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'category' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/category/:id',
                    'defaults' => [
                        'controller' => Controller\CategoryController::class,
                        'action' => 'index',
                    ],
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'wildcard' => [
                        'type' => 'wildcard',
                        'may_terminate' => true,
                        'options' => [
                            'key_value_delimiter' => '/',
                            'param_delimiter' => '/'
                        ],
                    ],
                ],
            ],
            'product' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:id',
                    'defaults' => [
                        'controller' => Controller\ProductController::class,
                        'action' => 'index',
                    ],
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'wildcard' => [
                        'type' => 'wildcard',
                        'may_terminate' => true,
                        'options' => [
                            'key_value_delimiter' => '/',
                            'param_delimiter' => '/'
                        ],
                    ],
                ],
            ],
            'compare' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/compare[/:action]',
                    'defaults' => [
                        'controller' => Controller\CompareController::class,
                        'action' => 'category',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'wildcard' => [
                        'type' => 'wildcard',
                        'may_terminate' => true,
                        'options' => [
                            'key_value_delimiter' => '/',
                            'param_delimiter' => '/'
                        ],
                    ],
                ],
            ],
            'cart' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cart[/:action]',
                    'defaults' => [
                        'controller' => Controller\CartController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'wildcard' => [
                        'type' => 'wildcard',
                        'may_terminate' => true,
                        'options' => [
                            'key_value_delimiter' => '/',
                            'param_delimiter' => '/'
                        ],
                    ],
                ],
            ],
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/login',
                    'defaults' => [
                        'controller' => Controller\LoginController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'controller' => Controller\LogoutController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'account' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/account',
                    'defaults' => [
                        'controller' => Controller\ProfileController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'agents' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/agents',
                            'defaults' => [
                                'controller' => Controller\AgentController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'create' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/create',
                                    'defaults' => [
                                        'controller' => Controller\AgentController::class,
                                        'action' => 'create',
                                    ],
                                    'constraints' => [
                                        'id' => '\d+'
                                    ]
                                ],
                            ],
                            'edit' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/edit/:id',
                                    'defaults' => [
                                        'controller' => Controller\AgentController::class,
                                        'action' => 'edit',
                                    ],
                                    'constraints' => [
                                        'id' => '\d+'
                                    ]
                                ],
                            ],
                        ],
                    ],
                    'addresses' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/addresses',
                            'defaults' => [
                                'controller' => Controller\AddressController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'create' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/create',
                                    'defaults' => [
                                        'controller' => Controller\AddressController::class,
                                        'action' => 'create',
                                    ],
                                    'constraints' => [
                                        'id' => '\d+'
                                    ]
                                ],
                            ],
                            'edit' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/edit/:id',
                                    'defaults' => [
                                        'controller' => Controller\AddressController::class,
                                        'action' => 'edit',
                                    ],
                                    'constraints' => [
                                        'id' => '\d+'
                                    ]
                                ],
                            ],
                            'delete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/delete/:id',
                                    'defaults' => [
                                        'controller' => Controller\AddressController::class,
                                        'action' => 'delete',
                                    ],
                                    'constraints' => [
                                        'id' => '\d+'
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
