<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ValidationErrors extends AbstractHelper
{
    public function __invoke($element)
    {
        $errors = $this->getErrors($element);

        if (empty($errors)) {
            return '';
        }

        $html = [];
        foreach ($errors as $error) {
            $html[] = '<p class="text-danger">' . $error . '</p>';
        }

        return join(PHP_EOL, $html);
    }

    protected function getErrors($element)
    {
        if (!isset($this->getView()->errors)) {
            return false;
        }

        $errors = $this->getView()->errors;

        if (isset($errors[$element])) {
            return $errors[$element];
        }

        return false;
    }
}