<?php

namespace Application;

use Application\Controller\Factory\AddressControllerFactory;
use Application\Controller\Factory\AgentControllerFactory;
use Application\Controller\Factory\CartControllerFactory;
use Application\Controller\Factory\CategoryControllerFactory;
use Application\Controller\Factory\CompareControllerFactory;
use Application\Controller\Factory\IndexControllerFactory;
use Application\Controller\Factory\LoginControllerFactory;
use Application\Controller\Factory\LogoutControllerFactory;
use Application\Controller\Factory\ProductControllerFactory;
use Application\Controller\Factory\ProfileControllerFactory;
use Application\View\Helper\ValidationErrors;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;
use Zend\ServiceManager\Factory\InvokableFactory;

class Module implements ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setLayout'], 100);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setLayout'], 100);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'checkIdentity'], 15000);
    }

    public function setLayout(MvcEvent $e)
    {
        $matches  = $e->getRouteMatch();

        if (!$matches instanceof RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        if (strpos($matches->getMatchedRouteName(), 'account') !== 0) {
            return;
        }

        $viewModel = $e->getViewModel();
        $viewModel->setTemplate('layout/account');
    }

    public function checkIdentity(MvcEvent $e)
    {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();

        if (!$matches instanceof RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        $availableActions = [
            'login', 'logout', 'register',
        ];

        if ((strpos($matches->getMatchedRouteName(), 'account') !== 0) ||
            in_array($matches->getParam('action'), $availableActions)
        ) {
            return;
        }

        $serviceLocator = $e->getApplication()->getServiceManager();
        $authService = $serviceLocator->get(AuthenticationService::class);

        if (!$authService->getIdentity()) {
            $response->getHeaders()->addHeaderLine('Location', '/');
            $response->setStatusCode(302);

            $e->stopPropagation(true);
            $e->setResponse($response);
            $e->setResult($response);
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\IndexController::class => IndexControllerFactory::class,
                Controller\CategoryController::class => CategoryControllerFactory::class,
                Controller\ProductController::class => ProductControllerFactory::class,
                Controller\CompareController::class => CompareControllerFactory::class,
                Controller\CartController::class => CartControllerFactory::class,
                Controller\LoginController::class => LoginControllerFactory::class,
                Controller\LogoutController::class => LogoutControllerFactory::class,
                Controller\ProfileController::class => ProfileControllerFactory::class,
                Controller\AgentController::class => AgentControllerFactory::class,
                Controller\AddressController::class => AddressControllerFactory::class
            ],
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'aliases' => [
                'validationErrors' => ValidationErrors::class
            ],
            'factories' => [
                ValidationErrors::class => InvokableFactory::class
            ],
        ];
    }
}
