<?php

namespace Application\Controller;

use Ultima\Clients\Entity\Address;
use Ultima\Clients\InputFilter\AddressInputFilter;
use Ultima\Clients\Repository\AddressRepositoryInterface;
use Zend\Hydrator\HydratorInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use UltimaClient\Client\Client as ApiClient;

class AddressController extends AbstractActionController
{
    private $addressRepository;

    private $apiClient;

    private $hydrator;

    public function __construct(AddressRepositoryInterface $addressRepository, HydratorInterface $hydrator, ApiClient $apiClient)
    {
        $this->addressRepository = $addressRepository;
        $this->hydrator = $hydrator;
        $this->apiClient = $apiClient;
    }

    public function indexAction()
    {
        $addresses = $this->addressRepository->getAddresses();

        return new ViewModel([
            'addresses' => $addresses
        ]);
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $viewModel = new ViewModel();

        $address = new Address();

        if ($request->isPost()) {
            $inputFilter = new AddressInputFilter();
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                $this->hydrator->hydrate($inputFilter->getValues(), $address);
                $result = $this->addressRepository->save($address);

                if ($result) {
                    $this->apiClient->getCache()->clearByPrefix($this->apiClient->getSsId());
                    $this->flashMessenger()->addSuccessMessage('Address has been created');

                    $this->redirect()->toRoute('account/addresses');
                } else {
                    $this->hydrator->hydrate($this->params()->fromPost(), $address);
                }
            } else {
                $this->hydrator->hydrate($this->params()->fromPost(), $address);
                $viewModel->setVariable('errors', $inputFilter->getMessages());
            }
        }

        $viewModel->setVariable('address', $address);

        return $viewModel;
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $addressId = $this->params()->fromRoute('id');

        $address = $this->addressRepository->getAddressById($addressId);

        $viewModel = new ViewModel();

        if ($request->isPost()) {
            $inputFilter = new AddressInputFilter();
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                $this->hydrator->hydrate($inputFilter->getValues(), $address);
                $result = $this->addressRepository->save($address);

                if ($result) {

                    $this->flashMessenger()->addSuccessMessage('Address has been updated');

                    $this->redirect()->toRoute('account/addresses');
                } else {
                    $this->hydrator->hydrate($this->params()->fromPost(), $address);
                }
            } else {
                $this->hydrator->hydrate($this->params()->fromPost(), $address);
                $viewModel->setVariable('errors', $inputFilter->getMessages());
            }
        }

        $viewModel->setVariable('address', $address);
        $viewModel->setTemplate('application/address/create');

        return $viewModel;
    }

    public function deleteAction()
    {
        $addressId = $this->params()->fromRoute('id');

        $address = $this->addressRepository->getAddressById($addressId);

        if ($this->addressRepository->remove($address)) {
            $this->apiClient->getCache()->clearByPrefix($this->apiClient->getSsId());
            $this->flashMessenger()->addSuccessMessage('Address has been removed');
        } else {
            $this->flashMessenger()->addErrorMessage('Address has not been removed');
        }

        return $this->redirect()->toRoute('account/addresses');
    }
}