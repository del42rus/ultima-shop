<?php

namespace Application\Controller;

use Ultima\Clients\Entity\Agent;
use Ultima\Clients\InputFilter\AgentInputFilter;
use Ultima\Clients\Repository\AgentRepositoryInterface;
use Zend\Hydrator\HydratorInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use UltimaClient\Client\Client as ApiClient;

class AgentController extends AbstractActionController
{
    private $agentRepository;

    private $apiClient;

    private $hydrator;

    public function __construct(AgentRepositoryInterface $agentRepository, HydratorInterface $hydrator, ApiClient $apiClient)
    {
        $this->agentRepository = $agentRepository;
        $this->hydrator = $hydrator;
        $this->apiClient = $apiClient;
    }

    public function indexAction()
    {
        $agents = $this->agentRepository->getAgents(true);

        return new ViewModel([
            'agents' => $agents
        ]);
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $viewModel = new ViewModel();

        $agent = new Agent();

        if ($request->isPost()) {
            $inputFilter = new AgentInputFilter();
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                $this->hydrator->hydrate($inputFilter->getValues(), $agent);
                $result = $this->agentRepository->save($agent);

                if ($result) {
                    $this->apiClient->getCache()->clearByPrefix($this->apiClient->getSsId());
                    $this->flashMessenger()->addSuccessMessage('Agent has been created');

                    $this->redirect()->toRoute('account/agents');
                } else {
                    $this->hydrator->hydrate($this->params()->fromPost(), $agent);
                }
            } else {
                $this->hydrator->hydrate($this->params()->fromPost(), $agent);
                $viewModel->setVariable('errors', $inputFilter->getMessages());
            }
        }

        $viewModel->setVariable('agent', $agent);

        return $viewModel;
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $agentId = $this->params()->fromRoute('id');

        $agent = $this->agentRepository->getAgentById($agentId, true);

        $viewModel = new ViewModel();

        if ($request->isPost()) {
            $inputFilter = new AgentInputFilter();
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                $this->hydrator->hydrate($inputFilter->getValues(), $agent);
                $result = $this->agentRepository->save($agent);

                if ($result) {
                    $this->apiClient->getCache()->clearByPrefix($this->apiClient->getSsId());
                    $this->flashMessenger()->addSuccessMessage('Agent has been updated');

                    $this->redirect()->toRoute('account/agents');
                } else {
                    $this->hydrator->hydrate($this->params()->fromPost(), $agent);
                }
            } else {
                $this->hydrator->hydrate($this->params()->fromPost(), $agent);
                $viewModel->setVariable('errors', $inputFilter->getMessages());
            }
        }

        $viewModel->setVariable('agent', $agent);
        $viewModel->setTemplate('application/agent/create');

        return $viewModel;
    }
}