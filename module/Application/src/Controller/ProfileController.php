<?php

namespace Application\Controller;

use Ultima\Clients\InputFilter\ClientInputFilter;
use Ultima\Clients\Repository\ClientRepositoryInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Hydrator\HydratorInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use UltimaClient\Client\Client as ApiClient;

class ProfileController extends AbstractActionController
{
    private $clientRepository;

    private $hydrator;

    private $apiClient;

    private $authService;

    public function __construct(
        ClientRepositoryInterface $clientRepository,
        HydratorInterface $hydrator,
        ApiClient $apiClient,
        AuthenticationService $authService
    ) {
        $this->clientRepository = $clientRepository;
        $this->hydrator = $hydrator;
        $this->apiClient = $apiClient;
        $this->authService = $authService;
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $client = $this->clientRepository->getClient(true);

        $viewModel = new ViewModel();

        if ($request->isPost()) {
            $inputFilter = new ClientInputFilter();
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                $this->hydrator->hydrate($inputFilter->getValues(), $client);

                $result = $this->clientRepository->save($client);

                if ($result) {
                    $this->apiClient->getCache()->clearByPrefix($this->apiClient->getSsId());
                    $this->authService->getStorage()->write(serialize($client));

                    $this->flashMessenger()->addSuccessMessage('Profile has been updated');

                    $this->redirect()->toRoute('account');
                } else {
                    $this->hydrator->hydrate($this->params()->fromPost(), $client);
                }
            } else {
                $this->hydrator->hydrate($this->params()->fromPost(), $client);
                $viewModel->setVariable('errors', $inputFilter->getMessages());
            }
        }

        $viewModel->setVariable('client', $client);

        return $viewModel;
    }
}