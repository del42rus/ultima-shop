<?php

namespace Application\Controller;

use Zend\Authentication\Adapter\ValidatableAdapterInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\InputFilter\LoginInputFilter;

class LoginController extends AbstractActionController
{
    /** @var AuthenticationService */
    private $authService;

    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    public function indexAction()
    {
        if ($this->authService->getIdentity()) {
            return $this->redirect()->toUrl('/');
        }

        $request = $this->getRequest();

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        if ($request->isPost()) {
            $inputFilter = new LoginInputFilter();
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                /** @var ValidatableAdapterInterface $authAdapter */
                $authAdapter = $this->authService->getAdapter();

                $authAdapter->setIdentity($request->getPost('email'));
                $authAdapter->setCredential($request->getPost('password'));

                $result = $this->authService->authenticate();

                if ($result->isValid()) {
                    return $this->redirect()->toUrl('/');
                }

                $viewModel->setVariables([
                    'messages' => $result->getMessages(),
                ]);
            }

            $viewModel->setVariables([
                'errors' => $inputFilter->getMessages(),
            ]);
        }

        return $viewModel;
    }
}