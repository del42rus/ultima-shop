<?php

namespace Application\Controller;

use Ultima\Catalog\Service\CatalogService;
use Zend\Mvc\Controller\AbstractActionController;

class ProductController extends AbstractActionController
{
    /**
     * @var CatalogService
     */
    private $catalogService;

    public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }

    public function indexAction()
    {

    }
}
