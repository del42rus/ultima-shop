<?php

namespace Application\Controller;

use Ultima\Catalog\Entity\Product;
use Ultima\Catalog\Service\CatalogService;
use Ultima\Order\Service\CartService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class CartController extends AbstractActionController
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CatalogService
     */
    private $catalogService;

    public function __construct(CartService $cartService, CatalogService $catalogService)
    {
        $this->cartService = $cartService;
        $this->catalogService = $catalogService;
    }

    public function indexAction()
    {
        $items = $this->cartService->getItems();

        return new ViewModel([
            'items' => $items
        ]);
    }

    public function addAction()
    {
        $productId = $this->params()->fromRoute('product');

        /** @var Product $product */
        $product = $this->catalogService->getProductById($productId);
        $product->setPrice($product->getPrice($this->prices()->getPriceCategoryId(), $this->location()->getZoneId()));

        $this->cartService->addProduct($product);

        return new JsonModel([
            'success' => 1,
            'quantity' => $this->cartService->getItemsCount(),
            'total' => $this->cartService->getTotal(),
        ]);
    }

    public function removeAction()
    {
        $productId = $this->params()->fromRoute('product');

        $result = $this->cartService->removeProduct($productId);

        return new JsonModel([
            'success' => $result,
            'quantity' => $this->cartService->getItemsQuantity(),
            'total' => $this->cartService->getTotal(),
        ]);
    }

    public function updateAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $quantity = $request->getPost('quantity');

        if (count($quantity)) {
            foreach ($quantity as $productId => $value) {
                $product = $this->catalogService->getProductById($productId);

                if (!$product instanceof Product) {
                    continue;
                }

                $this->cartService->addProduct($product, $value);
            }
        }

        $this->redirect()->toRoute('cart');
    }
}