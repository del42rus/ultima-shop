<?php

namespace Application\Controller;

use Ultima\Catalog\Filter\FilterSet;
use Ultima\Catalog\Filter\RangeFilter;
use Ultima\Catalog\Service\CatalogService;
use Ultima\Catalog\Service\FiltersService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CategoryController extends AbstractActionController
{
    /**
     * @var CatalogService
     */
    private $catalogService;

    /**
     * @var FiltersService
     */
    protected $filtersService;

    public function __construct(CatalogService $catalogService, FiltersService $filtersService)
    {
        $this->catalogService = $catalogService;
        $this->filtersService = $filtersService;
    }

    public function indexAction()
    {
        $request = $this->getRequest();

        $categoryId = $this->params()->fromRoute('id');
        $page = $this->params()->fromRoute('page', 1);

        $locationId = $this->location()->getId();
        $zoneId = $this->location()->getZoneId();
        $priceCategoryId = $this->prices()->getPriceCategoryId();

        $filterSet = new FilterSet();

        $this->filtersService->setupPriceFilter($filterSet, $categoryId, null, $locationId, $zoneId, $priceCategoryId);
        $this->filtersService->extractSelectedFilters($filterSet, $request->getQuery()->toArray());

        /** @var RangeFilter $priceFilter */
        $priceFilter = $filterSet->getFilter('price');
        $priceRange = $priceFilter->getSelectRange();

        $availableProductIds = $this->catalogService->getAvailableProductIds($categoryId, $locationId, $priceCategoryId, $priceRange);

        if (count($availableProductIds)) {
            $this->filtersService->setupFeatureFilters($filterSet, $availableProductIds);
            $this->filtersService->setupBrandsFilter($filterSet, $availableProductIds);

            $this->filtersService->extractSelectedFilters($filterSet, $request->getQuery()->toArray());
        }

        $productIds = $filterSet->hasSelectedFilters() ? $filterSet->getSelectedProductIds() : $filterSet->getProductIds();

        if (count($productIds)) {
            $sorting = [
                'column' => $request->getQuery('sortBy'),
                'direction' => $request->getQuery('sortDir')
            ];

            $products = $this->catalogService->getAvailableProductsByIds($productIds, $locationId, $priceCategoryId, $sorting, $page, 10);
        }

        $category = $this->catalogService->getCategoryById($categoryId);

        return new ViewModel([
            'category' => $category,
            'categoryId' => $categoryId,
            'products' => $products ?? [],
            'filters' => $filterSet,
            'sortingColumn' => $sorting['column'] ?? null,
            'sortingDirection' => $sorting['direction'] ?? null,
        ]);
    }
}