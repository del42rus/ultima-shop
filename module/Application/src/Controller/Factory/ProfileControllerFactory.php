<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Clients\Repository\ClientRepository;
use UltimaClient\Client\Client as ApiClient;
use Zend\Authentication\AuthenticationService;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class ProfileControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $clientRepository = $container->get(ClientRepository::class);
        $hydrator = new ClassMethods();
        $apiClient = $container->get(ApiClient::class);
        $authService = $container->get(AuthenticationService::class);

        return new Controller\ProfileController($clientRepository, $hydrator, $apiClient, $authService);
    }
}

