<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Entity\SiteCategory;
use Ultima\Catalog\Repository\SiteCategoryRepositoryInterface;
use Ultima\Catalog\Service\ComparisonService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CompareControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ComparisonService $compareService */
        $comparisonService = $container->get(ComparisonService::class);

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        /** @var SiteCategoryRepositoryInterface $siteCategoryRepository */
        $siteCategoryRepository = $entityManager->getRepository(SiteCategory::class);

        return new Controller\CompareController($comparisonService, $siteCategoryRepository);
    }
}

