<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Service\CatalogService;
use Ultima\Order\Service\CartService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CartControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var CartService $cartService */
        $cartService = $container->get(CartService::class);
        /** @var CatalogService $catalogService */
        $catalogService = $container->get(CatalogService::class);

        return new Controller\CartController($cartService, $catalogService);
    }
}

