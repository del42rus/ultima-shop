<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Service\CatalogService;
use Zend\ServiceManager\Factory\FactoryInterface;

class ProductControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var CatalogService $catalogService */
        $catalogService = $container->get(CatalogService::class);

        return new Controller\ProductController($catalogService);
    }
}

