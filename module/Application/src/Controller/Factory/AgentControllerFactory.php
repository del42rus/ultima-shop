<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Clients\Repository\AgentRepository;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class AgentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $agentRepository = $container->get(AgentRepository::class);
        $hydrator = new ClassMethods();
        $apiClient = $container->get(ApiClient::class);

        return new Controller\AgentController($agentRepository, $hydrator, $apiClient);
    }
}

