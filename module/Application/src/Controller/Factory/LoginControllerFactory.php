<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoginControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var AuthenticationService $authService */
        $authService = $container->get('ultima.authenticationservice.default');

        return new Controller\LoginController($authService);
    }
}

