<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Clients\Repository\AddressRepository;
use UltimaClient\Client\Client as ApiClient;
use Zend\Hydrator\ClassMethods;
use Zend\ServiceManager\Factory\FactoryInterface;

class AddressControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $addressRepository = $container->get(AddressRepository::class);
        $hydrator = new ClassMethods();
        $apiClient = $container->get(ApiClient::class);

        return new Controller\AddressController($addressRepository, $hydrator, $apiClient);
    }
}

