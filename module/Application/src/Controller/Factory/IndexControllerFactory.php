<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Clients\Repository\AddressRepository;
use Ultima\Clients\Repository\AgentRepository;
use Ultima\Clients\Repository\ClientRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $clientRepository = $container->get(ClientRepository::class);
        $addressRepository = $container->get(AddressRepository::class);
        $agentRepository = $container->get(AgentRepository::class);

        return new Controller\IndexController($clientRepository, $addressRepository, $agentRepository);
    }
}

