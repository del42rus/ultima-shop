<?php

namespace Application\Controller\Factory;

use Application\Controller;
use Interop\Container\ContainerInterface;
use Ultima\Catalog\Service\CatalogService;
use Ultima\Catalog\Service\FiltersService;
use Zend\ServiceManager\Factory\FactoryInterface;

class CategoryControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var CatalogService $catalogService */
        $catalogService = $container->get(CatalogService::class);
        /** @var FiltersService $filtersService */
        $filtersService = $container->get(FiltersService::class);

        return new Controller\CategoryController($catalogService, $filtersService);
    }
}

