<?php

namespace Application\Controller;

use Zend\Authentication\Adapter\ValidatableAdapterInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\InputFilter\LoginInputFilter;

class LogoutController extends AbstractActionController
{
    /** @var AuthenticationService */
    private $authService;

    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    public function indexAction()
    {
        $this->authService->clearIdentity();

        return $this->redirect()->toUrl('/');
    }
}