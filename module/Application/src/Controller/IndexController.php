<?php

namespace Application\Controller;

use Ultima\Clients\Repository\AddressRepositoryInterface;
use Ultima\Clients\Repository\AgentRepositoryInterface;
use Ultima\Clients\Repository\ClientRepositoryInterface;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    private $clientRepository;

    private $addressRepository;

    private $agentRepository;

    public function __construct(
        ClientRepositoryInterface $clientRepository,
        AddressRepositoryInterface $addressRepository,
        AgentRepositoryInterface $agentRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->addressRepository = $addressRepository;
        $this->agentRepository = $agentRepository;
    }

    public function indexAction()
    {

    }
}
