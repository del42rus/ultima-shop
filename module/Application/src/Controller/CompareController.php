<?php

namespace Application\Controller;

use Ultima\Catalog\Entity\SiteCategory;
use Ultima\Catalog\Repository\SiteCategoryRepositoryInterface;
use Ultima\Catalog\Service\CompareService;
use Ultima\Catalog\Service\ComparisonService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class CompareController extends AbstractActionController
{
    /**
     * @var ComparisonService
     */
    private $comparisonService;

    /**
     * @var SiteCategoryRepositoryInterface
     */
    private $siteCategoryRepository;

    public function __construct(ComparisonService $comparisonService, SiteCategoryRepositoryInterface $siteCategoryRepository)
    {
        $this->comparisonService = $comparisonService;
        $this->siteCategoryRepository = $siteCategoryRepository;
    }

    public function categoryAction()
    {
        $categoryId = $this->params()->fromRoute('id');

        if (!$categoryId) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $products = $this->comparisonService->getProducts($categoryId);
        $properties = $this->comparisonService->getProductsProperties($categoryId);

        return new ViewModel([
            'products' => $products,
            'productsProperties' => $properties,
            'categoryId' => $categoryId
        ]);
    }

    public function addAction()
    {
        $request = $this->getRequest();

        $productId = $request->getQuery('productId');
        $categoryId = $request->getQuery('categoryId');

        if (!$productId || !$categoryId) {
            return new JsonModel([
                'success' => 0
            ]);
        }

        $category = $this->siteCategoryRepository->getCategoryById($categoryId);

        $this->comparisonService->add($productId, $categoryId, $category->getName());

        if (!$category instanceof SiteCategory) {
            return new JsonModel([
                'success' => 0
            ]);
        }

        return new JsonModel([
            'success' => 1
        ]);
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        $productId = $request->getQuery('productId');
        $categoryId = $request->getQuery('categoryId');

        if (!$productId || !$categoryId) {
            return new JsonModel([
                'success' => 0
            ]);
        }

        $result = $this->comparisonService->remove($productId, $categoryId);

        if (!$result) {
            return new JsonModel([
                'success' => 0
            ]);
        }

        return new JsonModel([
            'success' => 1
        ]);
    }
}