<?php

namespace Application\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

class LoginInputFilter extends InputFilter
{
    public function __construct()
    {
        $email = new Input('email');
        $email->setRequired(true)
            ->getValidatorChain()
            ->attach(new Validator\EmailAddress());

        $password = new Input('password');
        $password
            ->setRequired(true)
            ->getValidatorChain()
            ->attach(new Validator\StringLength(7));

        $this->add($email)
            ->add($password);
    }
}