<?php

namespace Application\Cache;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Redis;

class RedisCacheFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $redis = new Redis();
        $redis->connect('localhost');

        return $redis;
    }
}